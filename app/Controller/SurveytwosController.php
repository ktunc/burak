<?php
/**
 * @property STwoU $STwoU
 * @property STwo $STwo
 * @property STwoG $STwoG
 * @property STwoL $STwoL
 * @property Question $Question
 * @property User $User
 */
class SurveytwosController extends AppController{
    public $name = "Surveytwos";
    public $uses = array('STwoU','STwo','STwoG','STwoL','Question','User');

    public $RandOyuncuArr;
    public $OyuncuSayisi = 10;
    public $Round = 10;

    public function index(){
        $named = $this->request->params['named'];
        if(!$this->Cookie->check('mId') || ($this->Cookie->check('mId') && array_key_exists('mId',$named) && !empty($named['mId']))){
            if(array_key_exists('mId',$named) && !empty($named['mId'])){
                $this->Cookie->destroy();
                $this->Cookie->write('mId',$named['mId']);
            }else{
                $this->redirect(array('controller'=>'pages'));
            }
        }

        $mId = $this->Cookie->read('mId');
        $user = $this->User->findByMid($mId);
        // mId daha önce sisteme kaydedilmiş mi
        $mIdVarmi = $this->STwoU->findByMid($mId);
        $oyundaMi = $this->STwo->find('all',array('conditions'=>array('OR'=>array('gonderen_mid'=>$mId,'cevaplayan_mid'=>$mId))));
		
        // Katılımcı sayısı tamam mı?
        $katilimciSayisi = $this->STwoU->find('count');

        if(!empty($user) && $user['User']['durum'] == 2){
            $this->redirect(array('controller'=>'surveytwos','action'=>'oyunbitti', 'm'=>$mId));
        }

        if(!empty($oyundaMi)){
            $this->redirect(array('controller'=>'surveytwos','action'=>'surveytwo', 'm'=>$mId, 'i'=>$mIdVarmi['STwoU']['id']));
        }

        $this->set('user',$user);
		$this->set('mIdVarmi',$mIdVarmi);
    }
	
    function OyuncuTamamMi(){
        $this->autoRender = false;
        $return = array('hata'=>true, 'asama'=>0, 'oyunsuresibaslat'=>false);
        $mId = $this->Cookie->read('mId');
        $user = $this->User->findByMid($mId);
        $userOyundaMi = $this->STwoU->find('first',array('conditions'=>array('mid'=>$mId)));

        $oyuncusayisi = $this->User->find('count',array('conditions'=>array('survey'=>2,'OR'=>array(array('durum'=>0),array('durum'=>1),array('durum'=>-1,"mid LIKE 'T1PC%'")))));
        $oyundakiOyuncuSayisi = $this->STwoU->find('count');

        if(empty($user) || $user['User']['durum'] == 2){
            $return['full'] = true;
        }else if($oyundakiOyuncuSayisi == $this->OyuncuSayisi && $userOyundaMi){
            $oyunVarMi = $this->STwoG->find('all');
            if($oyunVarMi){
                $return['hata'] = false;
                $return['asama'] = 1;
            }else{
                $this->urloyunac();
            }
            $return['m'] = $userOyundaMi['STwoU']['mid'];
            $return['i'] = $userOyundaMi['STwoU']['id'];
        }else if($oyundakiOyuncuSayisi == $this->OyuncuSayisi && empty($userOyundaMi)){
            $return['full'] = true;
        }else if(empty($userOyundaMi)){
            $pd = $this->userkaydet();
            if($pd){
                $userOyundaMi = $this->STwoU->find('first',array('conditions'=>array('mid'=>$mId, 'durum'=>1)));
                $return['m'] = $userOyundaMi['STwoU']['mid'];
                $return['i'] = $userOyundaMi['STwoU']['id'];
            }
        }

        if($userOyundaMi){
            if($oyuncusayisi >= $this->OyuncuSayisi){
                $return['oyunsuresibaslat'] = true;
            }
            $return['m'] = $userOyundaMi['STwoU']['mid'];
            $return['i'] = $userOyundaMi['STwoU']['id'];
            $mesaj = '<h5>You have been assigned 1 direct link';
            if($userOyundaMi['STwoU']['link'] == 1){
                $mesaj .= ' and 1 indirect link(s) for the entire game';
            }
            $mesaj .= '.</h5>';
            $return['mesaj'] = $mesaj;
        }

        echo json_encode($return);
        //return false;
    }
	
	private function userkaydet(){
		$this->autoRender = false;
		$mId = $this->Cookie->read('mId');
        $user = $this->User->findByMid($mId);
        // mId daha önce sisteme kaydedilmiş mi
        $mIdVarmi = $this->STwoU->findByMid($mId);
        $oyundaMi = $this->STwo->find('all',array('conditions'=>array('OR'=>array('gonderen_mid'=>$mId,'cevaplayan_mid'=>$mId))));
        // Katılımcı sayısı tamam mı?
        $katilimciSayisi = $this->STwoU->find('count');

        if(empty($user)){
            return false;
        }else if(empty($mIdVarmi)){
            if($katilimciSayisi == $this->OyuncuSayisi){
                return false;
            }else{
                $link = 0;
                $linksifir = $this->STwoU->find('count',array('conditions'=>array('link'=>0)));
                if($linksifir >= $this->OyuncuSayisi/2){
                    $link = 1;
                }
				$code = uniqid('2');
                $saved = array('mid'=>$mId, 'ip'=> $this->request->clientIp(), 'code'=>$code, 'link'=>$link, 'tarih'=>date('Y-m-d H:i:s'), 'durum'=>1);
                $this->STwoU->create();
                if($this->STwoU->save($saved)){
                    $id = $this->STwoU->getLastInsertID();
                    $this->STwoU->save(array('userid'=>$id));
                    $user = $this->User->findByMid($mId);
					$this->User->id = $user['User']['id'];
					$this->User->save(array('durum'=>1, 'zaman'=>1, 'survey'=>2, 'code'=>$code, 'ip'=> $this->request->clientIp()));
					return true;
                }else{
                    return false;
                }
            }
        }else if(empty($mIdVarmi)){
            return false;
        }else if(!empty($oyundaMi)){
            return false;
        }
	}

    public function surveytwo(){
		$named = $this->request->params['named'];
        if(!$this->Cookie->check('mId')){
            $this->redirect(array('controller'=>'pages'));
        }else if(!array_key_exists('m',$named) || !array_key_exists('i',$named) || empty($named['m']) || empty($named['i'])){
			$this->redirect(array('controller'=>'surveytwos','action'=>'index'));
		}
		
		$mId = $this->Cookie->read('mId');
		$m = $named['m'];
		$i = $named['i'];
		if($mId != $m){
			$tt = $this->STwoU->findByMidAndId($mId,$i);
			$pp = $this->STwoU->findByMidAndId($m,$i);
			if(!empty($tt)){
				$this->Cookie->write('mId',$mId);
				$m = $mId;
			}else if(!empty($pp)){
				$this->Cookie->write('mId',$m);
				$mId = $m;
			}else{
				$this->redirect(array('controller'=>'surveytwos','action'=>'index'));
			}
		}
		
        // Oyun Bitti Mi
        $sonOyun = $this->STwoG->find('first',array('conditions'=>array('asama != 0'),'order'=>array('oyun'=>'DESC')));
        if($sonOyun && $sonOyun['STwoG']['oyun'] >= $this->Round && $sonOyun['STwoG']['asama'] == 5){
            /*$oyunBittiMi = $this->STwo->find('first',array('conditions'=>array('cevaplayan_tarih IS NULL','oyun'=>$this->Round, 'asama'=>4)));
            if(empty($oyunBittiMi)){
                $this->redirect(array('controller'=>'surveys','action'=>'oyunbitti'));
            }*/
            $this->redirect(array('controller'=>'surveytwos','action'=>'question','m'=>$mId, 'i'=>$i));
        }
        // Oyun Bitti Mi SON

        $mUser = $this->STwoU->findByMid($mId);
        if($mUser){
            $asama = $sonOyun['STwoG']['asama'];
            $bekle = 0;

            $gMi = $this->STwo->find('first',array('conditions'=>array('gonderen_mid'=>$mId,'oyun'=>$sonOyun['STwoG']['oyun'])));
            $cMi = $this->STwo->find('first',array('conditions'=>array('cevaplayan_mid'=>$mId,'oyun'=>$sonOyun['STwoG']['oyun'])));

            if($asama == 1){
                if($gMi){
                    if($gMi['STwo']['asama'] == 1){
                        $bekle = 0;
                    }else{
                        $bekle = 1;
                    }
                    $reBilgi = $this->STwoG->find('all',
                        array(
                            'fields'=>array('STwoG.*','STwo.*','STwoU.*'),
                            'conditions'=>array('STwoG.asama'=>5),
                            'order'=>array('STwoG.oyun'=>'ASC'),
                            'joins'=>array(
                                array(
                                    'table'=>'stwo',
                                    'alias'=>'STwo',
                                    'type'=>'LEFT',
                                    'conditions'=>array('STwoG.oyun = STwo.oyun', 'STwo.cevaplayan_mid'=>$gMi['STwo']['cevaplayan_mid'])
                                ),
                                array(
                                    'table'=>'stwou',
                                    'alias'=>'STwoU',
                                    'type'=>'LEFT',
                                    'conditions'=>array('STwo.cevaplayan_mid = STwoU.mid')
                                )
                            )
                        )
                    );
                    $receiver = $this->STwoU->findByMid($gMi['STwo']['cevaplayan_mid']);
                    $this->set('receiver',$receiver);
                    $this->set('reBilgi',$reBilgi);
                }else if($cMi){
                    $bekle = 1;
                }
            }else if($asama == 2){
                if($cMi){
                    if($cMi['STwo']['asama'] == 2){
                        $bekle = 0;
                    }else{
                        $bekle = 1;
                    }
                    $sender = $this->STwoU->findByMid($cMi['STwo']['gonderen_mid']);
                    $this->set('sender',$sender);
                }else if($gMi){
                    $bekle = 1;
                }
            }else if($asama == 3){
                if($gMi){
                    if($gMi['STwo']['asama'] == 3){
                        $bekle = 0;
                    }else{
                        $bekle = 1;
                    }
                    $receiver = $this->STwoU->findByMid($gMi['STwo']['cevaplayan_mid']);
                    $this->set('receiver',$receiver);
                }else if($cMi){
                    if($cMi['STwo']['asama'] == 3){
                        $bekle = 0;
                    }else{
                        $bekle = 1;
                    }
                    $sender = $this->STwoU->findByMid($cMi['STwo']['gonderen_mid']);
                    $this->set('sender',$sender);
                    $conditions = array('cevaplayan_mid'=>$mId, 'asama'=>5);
                    $countRec = $this->STwo->find('count',array('conditions'=>$conditions));
                    $this->set('countRec',$countRec);
                }
            }else if($asama == 4){
                $dLinkUsers = $this->STwoL->find('all',
                    array(
                        'fields'=>array('STwoU.*', 'STwoL.*', 'STwo.gonderen_tutar','STwo.cevaplayan_tutar'),
                        'conditions'=>array('STwoL.mid'=>$mId, 'STwoL.link'=>1),
                        'joins'=>array(
                            array(
                                'table' => 'stwo',
                                'alias' => 'STwo',
                                'type' => 'INNER',
                                'conditions' => array('STwoL.lid = STwo.cevaplayan_mid','oyun'=>$sonOyun['STwoG']['oyun'])
                            ),
                            array(
                                'table' => 'stwou',
                                'alias' => 'STwoU',
                                'type' => 'INNER',
                                'conditions' => array('STwo.cevaplayan_mid = STwoU.mid')
                            )
                        )
                    )
                );

                $indLinkUsers = $this->STwoL->find('all',
                    array(
                        'fields'=>array('STwoU.*', 'STwoL.*', 'STwo.gonderen_tutar','STwo.cevaplayan_tutar'),
                        'conditions'=>array('STwoL.mid'=>$mId, 'STwoL.link'=>0),
                        'joins'=>array(
                            array(
                                'table' => 'stwo',
                                'alias' => 'STwo',
                                'type' => 'INNER',
                                'conditions' => array('STwoL.lid = STwo.cevaplayan_mid','oyun'=>$sonOyun['STwoG']['oyun'])
                            ),
                            array(
                                'table' => 'stwou',
                                'alias' => 'STwoU',
                                'type' => 'INNER',
                                'conditions' => array('STwo.cevaplayan_mid = STwoU.mid')
                            )
                        )
                    )
                );

                $this->set('indLinkUsers',$indLinkUsers);
                $this->set('dLinkUsers',$dLinkUsers);
            }

            $linkedUser = $this->STwoL->find('first',
                array(
                    'fields'=>array('STwoU.*'),
                    'conditions'=>array('STwoL.mid'=>$mId, 'STwoL.link'=>1),
                    'joins'=>array(
                        array(
                            'table'=>'stwou',
                            'alias'=>'STwoU',
                            'type'=>'INNER',
                            'conditions'=>array('STwoL.lid = STwoU.mid')
                        )
                    )
                )
            );
            $this->set('linkedUser',$linkedUser);

            $this->set('gonderimAsamasi',$gMi);
            $this->set('cevapmAsamasi',$cMi);
            $this->set('asama',$asama);
            $this->set('bekle',$bekle);
            $this->set('game',$sonOyun['STwoG']['oyun']);
            $this->set('mUser',$mUser);
        }else{
			$this->redirect(array('controller'=>'pages'));
        }
    }

    public function ParaGonder(){
        $this->autoRender = false;
        $return = array('hata'=>true);
        if($this->request->is('post')){
            $mId = $this->Cookie->read('mId');
            $data = $this->request->data;
            $tutar = $data['tutar'];
			$m = $data['m'];
			$i = $data['i'];
			if($mId != $m){
				$tt = $this->STwoU->findByMidAndId($mId,$i);
				$pp = $this->STwoU->findByMidAndId($m,$i);
				if(!empty($tt)){
					$this->Cookie->write('mId',$mId);
					$m = $mId;
				}else if(!empty($pp)){
					$this->Cookie->write('mId',$m);
					$mId = $m;
				}else{
					echo json_encode($return);
					exit();
				}
			}

            $bilgi = $this->STwo->find('first',array('conditions'=>array('gonderen_mid'=>$mId, 'gonderen_tarih IS NULL','asama'=>1)));
			if($bilgi["STwo"]["oyun"] == 1){
				$tutar = 30;
			}
            $saveData = array('gonderen_tutar'=>$tutar,'gonderen_tarih'=>date('Y-m-d H:i:s'),'asama'=>2);
            $this->STwo->id = $bilgi['STwo']['id'];
            if($this->STwo->save($saveData)){
                $return['hata'] = false;
            }
        }
        echo json_encode($return);
    }
    public function ParaKabulEt(){
        $this->autoRender = false;
        $return = array('hata'=>true);
        if($this->request->is('post')){
            $mId = $this->Cookie->read('mId');
            $data = $this->request->data;
            $tutar = str_replace(',','.',$data['tutar']);
			$m = $data['m'];
			$i = $data['i'];
			if($mId != $m){
				$tt = $this->STwoU->findByMidAndId($mId,$i);
				$pp = $this->STwoU->findByMidAndId($m,$i);
				if(!empty($tt)){
					$this->Cookie->write('mId',$mId);
					$m = $mId;
				}else if(!empty($pp)){
					$this->Cookie->write('mId',$m);
					$mId = $m;
				}else{
					echo json_encode($return);
					exit();
				}
			}

            $bilgi = $this->STwo->find('first',array('conditions'=>array('cevaplayan_mid'=>$mId, 'cevaplayan_tarih IS NULL','asama'=>2)));
			if($bilgi["STwo"]["oyun"] == 1){
				$tutar = 27;
			}
            $saveData = array('cevaplayan_tutar'=>$tutar,'cevaplayan_tarih'=>date('Y-m-d H:i:s'),'asama'=>3);
            $this->STwo->id = $bilgi['STwo']['id'];
            if($this->STwo->save($saveData)){
                $return['hata'] = false;

            }
        }
        echo json_encode($return);
    }
	
    function OyunSuresiDoldu(){
        $this->autoRender = false;
		$return = array('hata'=>true);
        if($this->request->is('post')){
            $mId = $this->Cookie->read('mId');
            $data = $this->request->data;
            $asama = $data['asama'];
            $oyun = $data['oyun'];
			
			$url = 'http://'.CURL_URL.'/curltwos/osdoldu/oyun:'.$oyun.'/asama:'.$asama;
			$curl = curl_init();                
			curl_setopt($curl, CURLOPT_URL, $url);
			curl_setopt($curl,  CURLOPT_RETURNTRANSFER, false);
			curl_exec($curl);   
			curl_close($curl);  
        }
        echo json_encode($return);
    }

    private function RandOyuncuArray($oyuncular){
        $this->autoRender = false;
        $this->RandOyuncuArr = array();
        //$randArray = array();
        shuffle($oyuncular);
        $gonderen = array_slice($oyuncular,0,$this->OyuncuSayisi/2);
        $cevaplayan = array_slice($oyuncular,$this->OyuncuSayisi/2,$this->OyuncuSayisi/2);
        foreach($gonderen as $grow){
            $conditions = array(
                "mid NOT IN (SELECT cevaplayan_mid FROM stwo WHERE gonderen_mid = '".$grow."')",
                "mid NOT IN (SELECT cevaplayan_mid FROM stwo WHERE cevaplayan_tarih IS NULL)",
                "mid !='".$grow."'",
                "mid IN ("."'".implode("','",array_values($cevaplayan))."')",
                !empty($this->RandOyuncuArr)?"mid NOT IN ("."'".implode("','",array_values($this->RandOyuncuArr))."')":"1 = 1"
            );
            $BostakiOyuncular = $this->STwoU->find('list',array('fields'=>array('mid'),'conditions'=>$conditions));
            $bosoyuncular = array_values($BostakiOyuncular);
            $bostaki = $bosoyuncular[rand(0,count($bosoyuncular)-1)];
            unset($cevaplayan[array_search($bostaki,$cevaplayan)]);
            $this->RandOyuncuArr[$grow] = $bostaki;
            if(empty($bosoyuncular)){
                $this->RandOyuncuArray($oyuncular);
            }
        }

        return $this->RandOyuncuArr;
    }

    function HangiAsama(){
        $this->autoRender = false;
        $return = array('hata'=>true);
        if($this->request->is('post')){
            $gonderimAsamasi = $this->STwo->find('first',array('conditions'=>array('gonderen_mid'=>$this->Cookie->read('mId'),'gonderen_tarih IS NULL')));
            if($gonderimAsamasi){
                $return['hata'] = false;
                $return['asama'] = 1;
                echo json_encode($return);
            }
            $cevapmAsamasi = $this->STwo->find('first',array('conditions'=>array('cevaplayan_mid'=>$this->Cookie->read('mId'),'cevaplayan_tarih IS NULL')));
            if($cevapmAsamasi){
                $return['hata'] = false;
                $return['asama'] = 2;
                $return['data'] = $cevapmAsamasi;
                echo json_encode($return);
            }

            echo json_encode($return);
        }
    }

    function AsamaBittiMi(){
        $this->autoRender = false;
        $return = array('sonuc'=>false);
        if($this->request->is('post')){
            $asama = $this->request->data('asama');
            $oyun = $this->request->data('oyun');
            $mId = $this->Cookie->read('mId');
            //$sonOyun = $this->STwoG->find('first',array('order'=>array('id'=>'DESC')));
            $conditions = array();
            if($asama == 1){
                $pat = $this->STwo->findAllByOyunAndAsama($oyun,1);
                if(empty($pat)){
                    $dat = $this->STwoG->findByOyunAndAsama($oyun,2);
                    if($dat){
                        $return['sonuc'] = true;
                    }else{
                        $stwog = $this->STwoG->findByOyun($oyun);
                        $this->STwoG->id = $stwog['STwoG']['id'];
                        $this->STwoG->save(array('asama'=>2));
                        $return['sonuc'] = true;
                    }
                }
            }else if($asama == 2){
                $pat = $this->STwo->findAllByOyunAndAsama($oyun,2);
                if(empty($pat)){
                    $dat = $this->STwoG->findByOyunAndAsama($oyun,3);
                    if($dat){
                        $return['sonuc'] = true;
                    }else{
                        $stwog = $this->STwoG->findByOyun($oyun);
                        $this->STwoG->id = $stwog['STwoG']['id'];
                        $this->STwoG->save(array('asama'=>3));
                        $return['sonuc'] = true;
                    }
                }
            }else if($asama == 3){
                $dat = $this->STwoG->findByOyunAndAsama($oyun,4);
                if($dat){
                    $return['sonuc'] = true;
                }
                //$conditions['OR'] = array('gonderen_tarih IS NULL','cevaplayan_tarih IS NULL');
            }else if($asama == 4){
                $dat = $this->STwoG->findByOyunAndAsama($oyun,5);
                if($dat){
                    $return['sonuc'] = true;
                }
            }
            //$sonuc = $this->STwo->find('first',array('conditions'=>$conditions));

            /*if((!$sonuc && ($asama == 1 || $asama == 2)) || ($sonuc && $asama == 0)){
                $return['sonuc'] = true;
            }*/
        }
        echo json_encode($return);
        return false;
    }

    function oyunbitti(){
		$named = $this->request->params['named'];
        if($this->Cookie->check('mId')){
            $mId = $this->Cookie->read('mId');
			$m = $named['m'];
            $i = array_key_exists('i',$named)?$named['i']:0;
			if($mId != $m){
				$tt = $this->STwoU->findByMidAndId($mId,$i);
				$pp = $this->STwoU->findByMidAndId($m,$i);
				if(!empty($tt)){
					$this->Cookie->write('mId',$mId);
					$m = $mId;
				}else if(!empty($pp)){
					$this->Cookie->write('mId',$m);
					$mId = $m;
				}else{
					$this->redirect(array('controller'=>'surveytwos','action'=>'index'));
				}
			}
            $userBilgi = $this->User->findByMid($mId);
            if($userBilgi){
                $this->set('hesTop',$userBilgi['User']['odenecek']);
                $this->set('code',$userBilgi['User']['code']);
//                $qq = $this->Question->findByMid($mId);
//                if($qq){
//                    $this->set('code',$userBilgi['STwoU']['code']);
//                }else{
//                    $this->redirect(array('controller'=>'surveytwos','action'=>'question'));
//                }
            }else{
                $this->redirect(array('controller'=>'pages'));
            }
        }
    }

    function sessifirla(){
        $this->autoRender = false;
        $this->Cookie->destroy();
        $this->redirect(array('controller'=>'sayfas'));
    }

    function question(){
        $this->autoRender = false;
		$named = $this->request->params['named'];
        if($this->Cookie->check('mId')){
            $mId = $this->Cookie->read('mId');
			$m = $named['m'];
			$i = $named['i'];
			if($mId != $m){
				$tt = $this->STwoU->findByMidAndId($mId,$i);
				$pp = $this->STwoU->findByMidAndId($m,$i);
				if(!empty($tt)){
					$this->Cookie->write('mId',$mId);
					$m = $mId;
				}else if(!empty($pp)){
					$this->Cookie->write('mId',$m);
					$mId = $m;
				}else{
					$this->redirect(array('controller'=>'surveytwos','action'=>'index'));
				}
			}
			$this->set('UserM',$m);
			$this->set('UserI',$i);
			// $hes = $this->STwo->query("SELECT sum(cevaplayan_tutar) as TOP_GELEN FROM stwo WHERE gdurum = 1 AND gonderen_mid = '".$mId."'");
			// $hes2 = $this->STwo->query("SELECT sum((gonderen_tutar*3)-cevaplayan_tutar) as TOP_GELEN FROM stwo WHERE cdurum = 1 AND cevaplayan_mid = '".$mId."'");
			$hes = $this->STwo->query("SELECT case when sum(gonderen_tutar*3) is null THEN 0 else sum(gonderen_tutar*3) end as TOP_GELEN FROM stwo WHERE gdurum = 1 AND gonderen_mid = '".$mId."'");
			$hes2 = $this->STwo->query("SELECT case when sum(cevaplayan_tutar) is null then 0 else sum(cevaplayan_tutar) end as TOP_GELEN FROM stwo WHERE cdurum = 1 AND cevaplayan_mid = '".$mId."'");
			$hesTop = (($hes[0][0]['TOP_GELEN'] + $hes2[0][0]['TOP_GELEN'])/300)*0.2;
			$this->set('hesTop',$hesTop);
			/* odenecek para miktari isle*/
			$user = $this->User->findByMid($mId);
			$this->User->id = $user['User']['id'];
			$this->User->save(array('odenecek'=>$hesTop));
			/* odenecek para miktari isle son*/
            $userBilgi = $this->STwoU->findByMid($mId);
            if($userBilgi){
//                $qq = $this->Question->findByMid($mId);
//                if($qq){
//                    $this->redirect(array('controller'=>'surveytwos','action'=>'oyunbitti','m'=>$mId,'i'=>$i));
//                }
                $pata = $this->STwoU->find('all');
                foreach ($pata as $row){
                    $taka = $this->User->findByMid($row['STwoU']['mid']);
                    $this->User->id = $taka['User']['id'];
                    $this->User->save(array('durum'=>2));
                }
                $this->redirect(array('controller'=>'surveytwos','action'=>'oyunbitti','m'=>$mId,'i'=>$i));
            }else{
                $this->redirect(array('controller'=>'pages'));
            }
        }else{
            $this->redirect(array('controller'=>'pages'));
        }
    }

    function questioncevap(){
        $this->autoRender = false;
        $return = array('hata'=>true,'sonuc'=>0);
        if($this->request->is('post')){
            $dat = $this->request->data;
            if(!$this->Cookie->check('mId')){
                // sayfadan at
                $return['sonuc'] = 3;
            }else{
				$mId = $this->Cookie->read('mId');
				$m = $dat['m'];
				$i = $dat['i'];
				if($mId != $m){
					$tt = $this->STwoU->findByMidAndId($mId,$i);
					$pp = $this->STwoU->findByMidAndId($m,$i);
					if(!empty($tt)){
						$this->Cookie->write('mId',$mId);
						$m = $mId;
					}else if(!empty($pp)){
						$this->Cookie->write('mId',$m);
						$mId = $m;
					}else{
						$return['sonuc'] = 3;
					}
				}
				$return['m'] = $m;
				$return['i'] = $i;
                $userBilgi = $this->STwoU->findByMid($mId);
                if($userBilgi && $return['sonuc'] == 0){
                    $qq = $this->Question->findByMid($mId);
                    if($qq){
                        // sonraki aşamaya at
                        $return['sonuc'] = 1;
                        $return['hata'] = false;
                    }else{
                        $dat['mid'] = $mId;
                        $dat['oyun'] = 2;
						unset($dat['m']);
						unset($dat['i']);
                        $this->Question->create();
                        if($this->Question->save($dat)){
                            // sonraki aşamaya at
                            $return['sonuc'] = 1;
                            $return['hata'] = false;
                        }else{
                            $return['sonuc'] = 2;
                        }
                    }
                }else{
                    $return['sonuc'] = 3;
                }
            }
        }

        echo json_encode($return);
    }

    public function indexsurekaydet(){
        $this->autoRender = false;
        if($this->request->is('post')){
            $time = $this->request->data('time');
            $mId = $this->Cookie->read('mId');
            $user = $this->User->findByMid($mId);
            $this->User->id = $user['User']['id'];
            if($this->User->save(array('zaman'=>$time))){
                echo json_encode(true);
            }else{
                echo json_encode(false);
            }
        }else{
            echo json_encode(false);
        }
    }
	
	private function urloyunac($oyun=1){
		$this->autoRender = false;
        $this->autoLayout = false;
		
		$url = 'http://'.CURL_URL.'/curltwos/YeniOyunAc/oyun:'.$oyun;
		$curl = curl_init();                
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl,  CURLOPT_RETURNTRANSFER, false);
		curl_exec($curl);   
		curl_close($curl);

        return false;
	}

    public function OyunuBaslat(){
        $this->autoRender = false;
        $this->autoLayout = false;
        if($this->request->is('post')){
            $data = $this->request->data;
            $midVarmi = $this->STwoU->findByIdAndMidAndDurum($data['i'],$data['m'],1);
            if($midVarmi){
				$this->User->deleteAll(array("mid IS NULL"),true);
                $pata = $this->User->findAllByDurumAndSurvey(0,2);
                foreach($pata as $row){
                    $oyuncusayisi = $this->STwoU->find('count');
                    $oyuncukayitlimi = $this->STwoU->findByMid($row['User']['mid']);
                    if(empty($oyuncukayitlimi) && $oyuncusayisi < $this->OyuncuSayisi){
                        $link = 0;
                        $linksifir = $this->STwoU->find('count',array('conditions'=>array('link'=>0)));
                        if($linksifir >= $this->OyuncuSayisi/2){
                            $link = 1;
                        }
                        $code = uniqid('2');
                        $saved = array('mid'=>$row['User']['mid'], 'ip'=> $this->request->clientIp(), 'code'=>$code, 'link'=>$link, 'tarih'=>date('Y-m-d H:i:s'), 'durum'=>1);
                        $this->STwoU->create();
                        if($this->STwoU->save($saved)) {
                            $id = $this->STwoU->getLastInsertID();
                            $this->STwoU->save(array('userid' => $id));
                            $user = $this->User->findByMid($row['User']['mid']);
                            $this->User->id = $user['User']['id'];
                            $this->User->save(array('durum' => 1, 'zaman' => 1, 'survey' => 2, 'code' => $code, 'ip' => $this->request->clientIp()));
                        }
                    }
                }
				
				$pata = $this->User->findAllByDurumAndSurvey(-1,2);
                foreach($pata as $row){
                    $oyuncusayisi = $this->STwoU->find('count');
                    $oyuncukayitlimi = $this->STwoU->findByMid($row['User']['mid']);
                    if(empty($oyuncukayitlimi) && $oyuncusayisi < $this->OyuncuSayisi){
                        $link = 0;
                        $linksifir = $this->STwoU->find('count',array('conditions'=>array('link'=>0)));
                        if($linksifir >= $this->OyuncuSayisi/2){
                            $link = 1;
                        }
                        $code = uniqid('2');
                        $saved = array('mid'=>$row['User']['mid'], 'ip'=> $this->request->clientIp(), 'code'=>$code, 'link'=>$link, 'tarih'=>date('Y-m-d H:i:s'), 'durum'=>1);
                        $this->STwoU->create();
                        if($this->STwoU->save($saved)) {
                            $id = $this->STwoU->getLastInsertID();
                            $this->STwoU->save(array('userid' => $id));
                            $user = $this->User->findByMid($row['User']['mid']);
                            $this->User->id = $user['User']['id'];
                            $this->User->save(array('durum' => 1, 'zaman' => 1, 'survey' => 2, 'code' => $code, 'ip' => $this->request->clientIp()));
                        }
                    }
                }
            }
        }
        echo json_encode(true);
    }
}