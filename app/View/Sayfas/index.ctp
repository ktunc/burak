<?php
if(!$mId){

}
else if(!$user){
    ?>
    <div class="container">
        <h3 class="text-center">You can't play this game.</h3>
        <div class="text-center">
            <h4>Please Wait...</h4>
        </div>
    </div>
    <script type="text/javascript">
        setTimeout(function(){window.location.reload();},30000);
    </script>
    <?php
} else {
    if($user['User']['survey'] == 2){
        $zaman = '14:00';
        $baslangic = 201703141400;
    }else if($user['User']['survey'] == 3){
        $zaman = '11:00 AM';
        $baslangic = 201705311100;
    }else{
        $zaman = '00:00';
        $baslangic = 0;
    }
    ?>
    <div class="container">
        <h3 class="text-center">Not Yet!</h3>
        <div class="text-center">
            <h4>Please Wait...</h4>
            <h4>Game will start at <?php echo $zaman; ?></h4>
            <h4>Time is <?php echo date('H:i'); ?></h4>
        </div>
    </div>
    <script type="text/javascript">
        setTimeout(function(){window.location.reload();},10000);
    </script>
    <?php
}
?>

<!-- Oyuncu Ekle Modal -->
<div id="MturkModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title text-center" id="myModalLabel">Please Write Your MTurk ID's</h3>
            </div>
            <div class="modal-body">
                <form id="OyuncuEkleForm" method="post" enctype="multipart/form-data" class="form-horizontal">
                    <fieldset>
                        <div class="form-group">
                            <label for="mid" class="col-lg-2 control-label">MTurk ID:</label>
                            <div class="col-lg-10">
                                <input type="text" name="mid" class="form-control" id="mid" placeholder="MTurk ID">
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-block btn-success" id="mTurkIsle">Submit</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function(){
	// $.alert('Please try again.');
	// $.alert({
		// title: 'Unfortunately, the session is full.',
		// content: 'Please contact me at <b>sloiac@essex.ac.uk</b> if you would like to know the time & day of our next session.',
		// type: 'red',
		// icon: 'fa fa-warning',
		// boxWidth:'50%',
		// useBootstrap: false,
		// buttons: {
			// ok:{
				// btnClass:'btn-primary',
				// action:function(){window.location.href = '<?php echo $this->Html->url('/');?>sayfas/full';}
			// }
		// }
	// });
    <?php if(!$mId){ ?>
    $('#MturkModal').modal({
        keyboard:false,
        backdrop:'static'
    });
    <?php }?>

    $('#MturkModal #mTurkIsle').on('click',function(){
        $.blockUI();
        $('#MturkModal').modal('hide');
        window.location.href = 'http://<?php echo ANA_URL;?>/sayfas/index/mId:'+$('#MturkModal #mid').val();
    });
});
</script>
