<div class="container">
	<h3 class="text-center">Unfortunately, the session is full.</h3>
	<h4 class="text-center">Please contact me at  <em>"sloiac@essex.ac.uk"</em>  if you would like to know the time & day of our next session.</h4>
</div>