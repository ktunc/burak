<?php
/**
 * @property SOneU $SOneU
 * @property SOne $SOne
 * @property SOneG $SOneG
 * @property STwoU $STwoU
 * @property STwo $STwo
 * @property STwoG $STwoG
 * @property STwoL $STwoL
 * @property SThreeU $SThreeU
 * @property SThree $SThree
 * @property SThreeG $SThreeG
 * @property SThreeL $SThreeL
 * @property Question $Question
 * @property SOneYedek $SOneYedek
 * @property STwoYedek $STwoYedek
 * @property SThreeYedek $SThreeYedek
 * @property Admin $Admin
 * @property User $User
 */
class SayfasController extends AppController
{
    public $name = "Sayfas";
    public $uses = array('SOneU','SOne','SOneG','SOneYedek',
        'STwoU','STwo','STwoG','STwoL','STwoYedek',
        'SThreeU','SThree','SThreeG','SThreeL','SThreeYedek',
        'Question','Admin','User');
	public $survey = 2;
	public $st1 = false;
	public $st2 = 201701010000;
	public $st3 = false;
	
	

    public function index(){
        $named = $this->request->params['named'];
        $user=false;
        if(!array_key_exists('mId',$named) || (array_key_exists('mId',$named) && empty($named['mId']))){
            $mId = false;
            $this->Cookie->destroy();
        }else{
            if(!$this->Cookie->check('mId') || ($this->Cookie->check('mId') && array_key_exists('mId',$named) && !empty($named['mId']))){
                if(array_key_exists('mId',$named) && !empty($named['mId'])){
                    $this->Cookie->destroy();
                    $this->Cookie->write('mId',strtoupper($named['mId']));
                }else{
                    $this->redirect(array('controller'=>'sayfas','action'=>'index'));
                }
            }

            $mId = $this->Cookie->read('mId');
            $user = $this->User->findByMid($mId);
            $midVarmi = false;
            $zaman = '00:00';
            $baslangic = 0;

            if(!empty($user)){
                if($user['User']['survey'] == 1){
					if($user['User']['durum'] == 2 || (date('YmdHi') >= $this->st1 && $this->st1)){
						$this->redirect(array('controller'=>'surveyones','action'=>'index', 'mId'=>$mId));
					}else{
						$this->redirect(array('controller'=>'sayfas','action'=>'index'));
					}
                    $midVarmi = $this->SOneU->findByMid($mId);
                }else if($user['User']['survey'] == 2){
					$midVarmi = $this->STwoU->findByMid($mId);
					if($user['User']['durum'] == 2 || (date('YmdHi') >= $this->st2 && $this->st2)){
						$this->redirect(array('controller'=>'surveytwos','action'=>'index', 'mId'=>$mId));
					}else{
						$this->redirect(array('controller'=>'sayfas','action'=>'index'));
					}
                }else if($user['User']['survey'] == 3){
					if($user['User']['durum'] == 2 || (date('YmdHi') >= $this->st3 && $this->st3)){
						$this->redirect(array('controller'=>'surveythrees','action'=>'index', 'mId'=>$mId));
					}else{
						$this->redirect(array('controller'=>'sayfas','action'=>'index'));
					}
                    $midVarmi = $this->SThreeU->findByMid($mId);
                }else{
					$this->redirect(array('controller'=>'sayfas','action'=>'index'));
                }
            }else{
				$donen = $this->oyuncuekle($mId, $this->survey);
				// $donen = $this->sthreeoyuncuekle($mId);
				if($donen){
					if($this->survey == 1){
						if(date('YmdHi') >= $this->st1 && $this->st1){
							$this->redirect(array('controller'=>'surveyones','action'=>'index', 'mId'=>$mId));
						}else{
							$this->redirect(array('controller'=>'sayfas','action'=>'index', 'mId'=>$mId));
						}
					}else if($this->survey == 2){
						if(date('YmdHi') >= $this->st2 && $this->st2){
							$this->redirect(array('controller'=>'surveytwos','action'=>'index', 'mId'=>$mId));
						}else{
							$this->redirect(array('controller'=>'sayfas','action'=>'index', 'mId'=>$mId));
						}
					}else if($this->survey == 3){
						if(date('YmdHi') >= $this->st3 && $this->st3){
							$this->redirect(array('controller'=>'surveythrees','action'=>'index', 'mId'=>$mId));
						}else{
							$this->redirect(array('controller'=>'sayfas','action'=>'index', 'mId'=>$mId));
						}
					}else{
						$this->redirect(array('controller'=>'sayfas','action'=>'index'));
					}
				}else{
					$this->Cookie->destroy();
					$this->redirect(array('controller'=>'sayfas','action'=>'index'));
				}
			}
        }

        $this->set('mId',$mId);
        $this->set('user',$user);
    }
	
	public function full(){}
	
	private function oyuncuekle($mId, $survey){
		$this->autoRender = false;
        if(mb_strlen($mId) > 8){
            $this->User->create();
            $save = array('mid'=>$mId,'survey'=>$survey);
            if($this->User->save($save)){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
	}
	
	private function sthreeoyuncuekle($mId){
        $this->autoRender = false;
        if(mb_strlen($mId) > 8){
            $this->User->create();
            $save = array('mid'=>$mId,'survey'=>3);
            if($this->User->save($save)){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
}