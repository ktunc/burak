<?php
?>
<style>
    .container{
        padding-top: 15px;
        padding-bottom: 15px;
    }
</style>
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <a class="btn btn-primary btn-xs" href="<?php echo $this->Html->url('/');?>admins/sone">İlk Oyun</a>
            <a class="btn btn-primary btn-xs" href="<?php echo $this->Html->url('/');?>admins/stwo">İkinci Oyun</a>
            <a class="btn btn-success" href="<?php echo $this->Html->url('/');?>admins/sthree">Üçüncü Oyun</a>
        </div>
    </div>
</div>
<div class="container">
    <hr>
</div>
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <button type="button" class="btn btn-xs btn-success" id="oyuncuekle"><i class="fa fa-plus"></i> Oyuncu Ekle</button>
        </div>
    </div>
</div>
<div class="container">
    <h3>Oyuncular</h3>
    <div class="row">
        <div class="col-xs-12">
            <table class="table table-striped table-hover table-bordered">
                <thead>
                <tr>
                    <th>MTurk ID</th>
                    <th>In Game</th>
                    <!--                    <th>Date</th>-->
                    <th>IP</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($oyuncular as $row){
                    echo '<tr>';
                    echo '<td>'.$row['User']['mid'].'</td>';
                    if($row['User']['durum'] == 1){
                        echo '<td>Yes</td>';
                    }else if($row['User']['durum'] == 2){
                        echo '<td>Finished!</td>';
                    }else{
                        echo '<td>Not now!</td>';
                    }
                    //echo '<td>'.$row['SOneU']['tarih'].'</td>';
                    echo '<td>'.$row['User']['ip'].'</td>';
                    echo '</tr>';
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="container">
    <hr>
</div>
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <button type="button" class="btn btn-xs btn-success" id="yedekle">Dataları Yedekle</button>
            <button type="button" class="btn btn-xs btn-danger" id="sifirla">Dataları Sıfırla</button>
        </div>
    </div>
</div>
<div class="container">
    <h3>Yedekler</h3>
    <div class="row">
        <div class="col-xs-12">
            <table class="table table-striped table-hover table-bordered">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Yedek Adı</th>
                    <th>Tarih</th>
                    <th>Oyun Dosyası</th>
                    <th>User Dosyası</th>
                    <th>Link Dosyası</th>
                    <th>DB Dosya</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($yedek as $row){
                    echo '<tr>';
                    echo '<td>'.$row['SThreeYedek']['id'].'</td>';
                    echo '<td>'.$row['SThreeYedek']['name'].'</td>';
                    echo '<td>'.$row['SThreeYedek']['tarih'].'</td>';
                    echo '<td><a target="_blank" href="'.$this->Html->url('/').'excel/sthree/'.$row['SThreeYedek']['oyun_path'].'">'.$row['SThreeYedek']['oyun_path'].'</a></td>';
                    echo '<td><a target="_blank" href="'.$this->Html->url('/').'excel/sthree/'.$row['SThreeYedek']['user_path'].'">'.$row['SThreeYedek']['user_path'].'</a></td>';
                    echo '<td><a target="_blank" href="'.$this->Html->url('/').'excel/sthree/'.$row['SThreeYedek']['link_path'].'">'.$row['SThreeYedek']['link_path'].'</a></td>';
                    echo '<td><a target="_blank" href="'.$this->Html->url('/').'sqlbackup/sthree/'.$row['SThreeYedek']['dbyedek'].'">'.$row['SThreeYedek']['dbyedek'].'</a></td>';
                    echo '</tr>';
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="container">
    <hr>
</div>
<div class="container">
    <h3>Oyun</h3>
    <div class="row">
        <div class="col-xs-12">
            <table class="table table-striped table-hover table-bordered">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>First Mover</th>
                    <th>Sent Money</th>
                    <th>First Durum</th>
                    <th>Round</th>
                    <th>Second Mover</th>
                    <th>Return Money</th>
                    <th>Second Durum</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($datas as $row){
                    echo '<tr>';
                    echo '<td>'.$row['SThree']['id'].'</td>';
                    echo '<td>'.$row['SThree']['gonderen_mid'].'</td>';
                    echo '<td>'.$row['SThree']['gonderen_tutar'].'</td>';
                    echo '<td>'.$row['SThree']['gdurum'].'</td>';
                    echo '<td>'.$row['SThree']['oyun'].'</td>';
                    echo '<td>'.$row['SThree']['cevaplayan_mid'].'</td>';
                    echo '<td>'.$row['SThree']['cevaplayan_tutar'].'</td>';
                    echo '<td>'.$row['SThree']['cdurum'].'</td>';
                    echo '</tr>';
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- Modal -->
<div id="YedekModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title text-center" id="myModalLabel">Yedekleme</h3>
            </div>
            <div class="modal-body">
                <form id="YedekleForm" action="<?php echo $this->Html->url('/');?>admins/sthreeyedekal" method="post" enctype="multipart/form-data" class="form-horizontal">
                    <fieldset>
                        <div class="form-group">
                            <label for="name" class="col-lg-2 control-label">Yedek İsmi</label>
                            <div class="col-lg-10">
                                <input type="text" name="name" class="form-control" id="name" placeholder="Yedek İsmi">
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-danger" id="YedekModalKapat">Kapat</button>
                <button type="button" class="btn btn-sm btn-success" id="YedekKaydet">Kaydet</button>
            </div>
        </div>
    </div>
</div>

<!-- Oyuncu Ekle Modal -->
<div id="OyuncuEkleModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title text-center" id="myModalLabel">Oyuncu Ekle</h3>
            </div>
            <div class="modal-body">
                <form id="OyuncuEkleForm" action="<?php echo $this->Html->url('/');?>admins/sthreeoyuncuekle" method="post" enctype="multipart/form-data" class="form-horizontal">
                    <fieldset>
                        <div class="form-group">
                            <label for="mid" class="col-lg-2 control-label">MTurk ID</label>
                            <div class="col-lg-10">
                                <input type="text" name="mid" class="form-control" id="mid" placeholder="MTurk ID">
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-danger" id="OyuncuEkleModalKapat">Kapat</button>
                <button type="button" class="btn btn-sm btn-success" id="OyuncuEkleKaydet">Kaydet</button>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $('#YedekModal #YedekModalKapat').on('click',function(){
            $('#YedekModal').modal('hide');
        });
        $('#yedekle').on('click',function(){
            $('#YedekModal').modal({
                keyboard:false,
                backdrop:'static'
            });
        });

        $('#YedekModal #YedekKaydet').on('click',function(){
            $.blockUI();
            $('#YedekModal').modal('hide');
            $('#YedekModal #YedekleForm').submit();
        });

        $('#sifirla').on('click',function(){
            if(confirm('Dataları sıfırlamak istediğinizden emin misiniz?')){
                $.blockUI();
                setTimeout(function(){
                    $.ajax({
                        async: false,
                        type: 'POST',
                        url: '<?php echo $this->Html->url('/');?>admins/sthreesifirla'
                    }).done(function(data){
                        window.location.reload();
                    }).fail(function(){
                        window.location.reload();
                    });
                },500);
            }
        });

        $('#oyuncuekle').on('click',function(){
            $('#OyuncuEkleModal').modal({
                keyboard:false,
                backdrop:'static'
            });
        });

        $('#OyuncuEkleModal #OyuncuEkleModalKapat').on('click',function(){
            $('#OyuncuEkleModal').modal('hide');
        });

        $('#OyuncuEkleModal #OyuncuEkleKaydet').on('click',function(){
            $.blockUI();
            $('#OyuncuEkleModal').modal('hide');
            $('#OyuncuEkleModal #OyuncuEkleForm').submit();
        });
    });
</script>