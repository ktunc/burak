<div class="container">
    <div class="row">
        <div class="col-xs-12 text-center">
            <h2>Your code: <?php echo $code; ?></h2>
        </div>
        <div class="col-xs-12 text-center">
            <h2>Your profit (as your bonus payment): $ <?php echo number_format($hesTop,2,',','.'); ?></h2>
        </div>
    </div>
</div>