<?php
/**
 * @property SThreeU $SThreeU
 * @property SThree $SThree
 * @property SThreeG $SThreeG
 * @property SThreeL $SThreeL
 * @property Question $Question
 * @property User $User
 */
class SurveythreesController extends AppController{
    public $name = "Surveythrees";
    public $uses = array('SThreeU','SThree','SThreeG','SThreeL','Question','User');

    public $RandOyuncuArr;
    public $OyuncuSayisi = 10;
    public $Round = 10;
	
	private function setCookie($key, $val){
		setcookie($key,$val,time() + (86400 * 30), "/");
		$_COOKIE[$key] = $val;
	}
	
	private function getCookie($key){
		return array_key_exists($key,$_COOKIE)?$_COOKIE[$key]:false;
	}
	
	private function checkCookie($key){
		return array_key_exists($key,$_COOKIE)?true:false;
	}
	
	private function deleteCookie($key){
		unset($_COOKIE[$key]);
	}
	
	private function destroyCookie(){
		foreach ($_COOKIE as $key=>$val) {
			pr($_COOKIE[$key]);
			pr($_COOKIE);
			exit();
			unset($_COOKIE[$key]);
		}
	}

    public function index(){
        $named = $this->request->params['named'];
        if(!$this->checkCookie('mId') || ($this->checkCookie('mId') && array_key_exists('mId',$named) && !empty($named['mId']))){
            if(array_key_exists('mId',$named) && !empty($named['mId'])){
                $this->destroyCookie();
                $this->setCookie('mId',$named['mId']);
            }else{
                $this->redirect(array('controller'=>'pages','action'=>'home'));
            }
        }

        $mId = $this->getCookie('mId');
        $user = $this->User->findByMid($mId);
        // mId daha önce sisteme kaydedilmiş mi
        $mIdVarmi = $this->SThreeU->findByMid($mId);
        $oyundaMi = $this->SThree->find('all',array('conditions'=>array('OR'=>array('gonderen_mid'=>$mId,'cevaplayan_mid'=>$mId))));

        // Katılımcı sayısı tamam mı?
        $katilimciSayisi = $this->SThreeU->find('count');

        if(!empty($user) && $user['User']['durum'] == 2){
            $this->redirect(array('controller'=>'surveythrees','action'=>'oyunbitti', 'm'=>$mId));
        }

		if(!empty($oyundaMi)){
            $this->redirect(array('controller'=>'surveythrees','action'=>'surveythree', 'm'=>$mId, 'i'=>$mIdVarmi['SThreeU']['id']));
        }

        $this->set('user',$user);
		$this->set('mIdVarmi',$mIdVarmi);
    }
	
	function OyuncuTamamMi(){
        $this->autoRender = false;
        $return = array('hata'=>true, 'asama'=>0, 'oyunsuresibaslat'=>false);
		$mId = $this->getCookie('mId');
		$user = $this->User->findByMid($mId);
        $userOyundaMi = $this->SThreeU->find('first',array('conditions'=>array('mid'=>$mId)));

        $oyuncusayisi = $this->User->find('count',array('conditions'=>array('survey'=>3,'OR'=>array(array('durum'=>0),array('durum'=>1),array('durum'=>-1,"mid LIKE 'T2PC%'")))));
        $oyundakiOyuncuSayisi = $this->SThreeU->find('count');

        if(empty($user) || $user['User']['durum'] == 2){
            $return['full'] = true;
        }else if($oyundakiOyuncuSayisi == $this->OyuncuSayisi && $userOyundaMi){
            $oyunVarMi = $this->SThreeG->find('all');
            if($oyunVarMi){
                $return['hata'] = false;
                $return['asama'] = 1;
            }else{
				$this->urloyunac();
            }
			$return['m'] = $userOyundaMi['SThreeU']['mid'];
			$return['i'] = $userOyundaMi['SThreeU']['id'];
        }else if($oyundakiOyuncuSayisi == $this->OyuncuSayisi && empty($userOyundaMi)){
			$return['full'] = true;
		}else if(empty($userOyundaMi)){
            $pd = $this->userkaydet();
            if($pd){
                $userOyundaMi = $this->SThreeU->find('first',array('conditions'=>array('mid'=>$mId, 'durum'=>1)));
                $return['m'] = $userOyundaMi['SThreeU']['mid'];
                $return['i'] = $userOyundaMi['SThreeU']['id'];
            }
		}

        if($userOyundaMi){
            if($oyuncusayisi >= $this->OyuncuSayisi){
                $return['oyunsuresibaslat'] = true;
            }
            $return['m'] = $userOyundaMi['SThreeU']['mid'];
            $return['i'] = $userOyundaMi['SThreeU']['id'];
            $mesaj = '<h5>You have been assigned 1 direct link';
            if($userOyundaMi['SThreeU']['link'] == 1){
                $mesaj .= ' and 5 indirect link(s) for the entire game';
            }
            $mesaj .= '.</h5>';
            $return['mesaj'] = $mesaj;
        }

        echo json_encode($return);
//        return false;
    }
	
	private function userkaydet(){
		$this->autoRender = false;
		$mId = $this->getCookie('mId');
        $user = $this->User->findByMid($mId);
        // mId daha önce sisteme kaydedilmiş mi
        $mIdVarmi = $this->SThreeU->findByMid($mId);
        $oyundaMi = $this->SThree->find('all',array('conditions'=>array('OR'=>array('gonderen_mid'=>$mId,'cevaplayan_mid'=>$mId))));
        // Katılımcı sayısı tamam mı?
        $katilimciSayisi = $this->SThreeU->find('count');

        if(empty($user)){
            return false;
        }else if(empty($mIdVarmi)){
            if($katilimciSayisi == $this->OyuncuSayisi){
                return false;
            }else{
                $link = 0;
                $linksifir = $this->SThreeU->find('count',array('conditions'=>array('link'=>0)));
                if($linksifir >= $this->OyuncuSayisi/2){
                    $link = 1;
                }
				$code = uniqid('2');
                $saved = array('mid'=>$mId, 'ip'=> $this->request->clientIp(), 'code'=>$code, 'link'=>$link, 'tarih'=>date('Y-m-d H:i:s'), 'durum'=>1);
                $this->SThreeU->create();
                if($this->SThreeU->save($saved)){
                    $id = $this->SThreeU->getLastInsertID();
                    $this->SThreeU->save(array('userid'=>$id));
                    $user = $this->User->findByMid($mId);
					$this->User->id = $user['User']['id'];
					$this->User->save(array('durum'=>1, 'zaman'=>1, 'survey'=>3, 'code'=>$code, 'ip'=> $this->request->clientIp()));
					return true;
                }else{
                    return false;
                }
            }
        }else if(empty($mIdVarmi)){
            return false;
        }else if(!empty($oyundaMi)){
            return false;
        }
	}

    public function surveythree(){
		$named = $this->request->params['named'];
        if(!$this->checkCookie('mId')){
            $this->redirect(array('controller'=>'pages','action'=>'home'));
        }else if(!array_key_exists('m',$named) || !array_key_exists('i',$named) || empty($named['m']) || empty($named['i'])){
			$this->redirect(array('controller'=>'surveythrees','action'=>'index'));
		}

        $mId = $this->getCookie('mId');
		$m = $named['m'];
		$i = $named['i'];
		if($mId != $m){
			$tt = $this->SThreeU->findByMidAndId($mId,$i);
			$pp = $this->SThreeU->findByMidAndId($m,$i);
			if(!empty($tt)){
				$this->setCookie('mId',$mId);
				$m = $mId;
			}else if(!empty($pp)){
				$this->setCookie('mId',$m);
				$mId = $m;
			}else{
				$this->redirect(array('controller'=>'surveythrees','action'=>'index'));
			}
		}
		
        // Oyun Bitti Mi
		$sonOyun = $this->SThreeG->find('first',array('conditions'=>array('asama != 0'),'order'=>array('id'=>'DESC')));
        if($sonOyun && $sonOyun['SThreeG']['id'] >= $this->Round && $sonOyun['SThreeG']['asama'] == 5){
            /*$oyunBittiMi = $this->SThree->find('first',array('conditions'=>array('cevaplayan_tarih IS NULL','oyun'=>$this->Round, 'asama'=>4)));
            if(empty($oyunBittiMi)){
                $this->redirect(array('controller'=>'surveys','action'=>'oyunbitti'));
            }*/
            $this->redirect(array('controller'=>'surveythrees','action'=>'question','m'=>$mId, 'i'=>$i));
        }
        // Oyun Bitti Mi SON

        $mUser = $this->SThreeU->findByMid($mId);
        if($mUser){
            $asama = $sonOyun['SThreeG']['asama'];
            $bekle = 0;

            $gMi = $this->SThree->find('first',array('conditions'=>array('gonderen_mid'=>$mId,'oyun'=>$sonOyun['SThreeG']['id'])));
            $cMi = $this->SThree->find('first',array('conditions'=>array('cevaplayan_mid'=>$mId,'oyun'=>$sonOyun['SThreeG']['id'])));

            if($asama == 1){
                if($gMi){
                    if($gMi['SThree']['asama'] == 1){
                        $bekle = 0;
                    }else{
                        $bekle = 1;
                    }
                    $reBilgi = $this->SThreeG->find('all',
                        array(
                            'fields'=>array('SThreeG.*','SThree.*','SThreeU.*'),
                            'conditions'=>array('SThreeG.asama'=>5),
                            'order'=>array('SThreeG.id'=>'ASC'),
                            'joins'=>array(
                                array(
                                    'table'=>'sthree',
                                    'alias'=>'SThree',
                                    'type'=>'LEFT',
                                    'conditions'=>array('SThreeG.id = SThree.oyun', 'SThree.cevaplayan_mid'=>$gMi['SThree']['cevaplayan_mid'])
                                ),
                                array(
                                    'table'=>'sthreeu',
                                    'alias'=>'SThreeU',
                                    'type'=>'LEFT',
                                    'conditions'=>array('SThree.cevaplayan_mid = SThreeU.mid')
                                )
                            )
                        )
                    );
                    $receiver = $this->SThreeU->findByMid($gMi['SThree']['cevaplayan_mid']);
                    $this->set('receiver',$receiver);
                    $this->set('reBilgi',$reBilgi);
                }else if($cMi){
                    $bekle = 1;
                }
            }else if($asama == 2){
                if($cMi){
                    if($cMi['SThree']['asama'] == 2){
                        $bekle = 0;
                    }else{
                        $bekle = 1;
                    }
                    $sender = $this->SThreeU->findByMid($cMi['SThree']['gonderen_mid']);
                    $this->set('sender',$sender);
                }else if($gMi){
                    $bekle = 1;
                }
            }else if($asama == 3){
                if($gMi){
                    if($gMi['SThree']['asama'] == 3){
                        $bekle = 0;
                    }else{
                        $bekle = 1;
                    }
                    $receiver = $this->SThreeU->findByMid($gMi['SThree']['cevaplayan_mid']);
                    $this->set('receiver',$receiver);
                }else if($cMi){
                    if($cMi['SThree']['asama'] == 3){
                        $bekle = 0;
                    }else{
                        $bekle = 1;
                    }
                    $sender = $this->SThreeU->findByMid($cMi['SThree']['gonderen_mid']);
                    $this->set('sender',$sender);
                    $conditions = array('cevaplayan_mid'=>$mId, 'asama'=>5);
                    $countRec = $this->SThree->find('count',array('conditions'=>$conditions));
                    $this->set('countRec',$countRec);
                }
            }else if($asama == 4){
                $dLinkUsers = $this->SThreeL->find('all',
                    array(
                        'fields'=>array('SThreeU.*', 'SThreeL.*', 'SThree.gonderen_tutar','SThree.cevaplayan_tutar'),
                        'conditions'=>array('SThreeL.mid'=>$mId, 'SThreeL.link'=>1),
                        'joins'=>array(
                            array(
                                'table' => 'sthree',
                                'alias' => 'SThree',
                                'type' => 'INNER',
                                'conditions' => array('SThreeL.lid = SThree.cevaplayan_mid','oyun'=>$sonOyun['SThreeG']['id'])
                            ),
                            array(
                                'table' => 'sthreeu',
                                'alias' => 'SThreeU',
                                'type' => 'INNER',
                                'conditions' => array('SThree.cevaplayan_mid = SThreeU.mid')
                            )
                        )
                    )
                );

                $indLinkUsers = $this->SThreeL->find('all',
                    array(
                        'fields'=>array('SThreeU.*', 'SThreeL.*', 'SThree.gonderen_tutar','SThree.cevaplayan_tutar'),
                        'conditions'=>array('SThreeL.mid'=>$mId, 'SThreeL.link'=>0),
                        'joins'=>array(
                            array(
                                'table' => 'sthree',
                                'alias' => 'SThree',
                                'type' => 'INNER',
                                'conditions' => array('SThreeL.lid = SThree.cevaplayan_mid','oyun'=>$sonOyun['SThreeG']['id'])
                            ),
                            array(
                                'table' => 'sthreeu',
                                'alias' => 'SThreeU',
                                'type' => 'INNER',
                                'conditions' => array('SThree.cevaplayan_mid = SThreeU.mid')
                            )
                        )
                    )
                );

                $this->set('indLinkUsers',$indLinkUsers);
                $this->set('dLinkUsers',$dLinkUsers);
            }

            $linkedUser = $this->SThreeL->find('first',
                array(
                    'fields'=>array('SThreeU.*'),
                    'conditions'=>array('SThreeL.mid'=>$mId, 'SThreeL.link'=>1),
                    'joins'=>array(
                        array(
                            'table'=>'sthreeu',
                            'alias'=>'SThreeU',
                            'type'=>'INNER',
                            'conditions'=>array('SThreeL.lid = SThreeU.mid')
                        )
                    )
                )
            );
            $this->set('linkedUser',$linkedUser);

            $this->set('gonderimAsamasi',$gMi);
            $this->set('cevapmAsamasi',$cMi);
            $this->set('asama',$asama);
            $this->set('bekle',$bekle);
            $this->set('game',$sonOyun['SThreeG']['id']);
            $this->set('mUser',$mUser);
        }else{
			$this->redirect(array('controller'=>'pages','action'=>'home'));
        }
    }

    public function ParaGonder(){
        $this->autoRender = false;
        $return = array('hata'=>true);
        if($this->request->is('post')){
            $mId = $this->getCookie('mId');
            $data = $this->request->data;
			$tutar = $data['tutar'];
			$m = $data['m'];
			$i = $data['i'];
			if($mId != $m){
				$tt = $this->SThreeU->findByMidAndId($mId,$i);
				$pp = $this->SThreeU->findByMidAndId($m,$i);
				if(!empty($tt)){
					$this->setCookie('mId',$mId);
					$m = $mId;
				}else if(!empty($pp)){
					$this->setCookie('mId',$m);
					$mId = $m;
				}else{
					echo json_encode($return);
					exit();
				}
			}
			
            $bilgi = $this->SThree->find('first',array('conditions'=>array('gonderen_mid'=>$mId, 'gonderen_tarih IS NULL','asama'=>1)));
			if($bilgi["SThree"]["oyun"] == 1){
				$tutar = 30;
			}
            $saveData = array('gonderen_tutar'=>$tutar,'gonderen_tarih'=>date('Y-m-d H:i:s'),'asama'=>2);
            $this->SThree->id = $bilgi['SThree']['id'];
            if($this->SThree->save($saveData)){
                $return['hata'] = false;
            }
        }
        echo json_encode($return);
    }
    public function ParaKabulEt(){
        $this->autoRender = false;
        $return = array('hata'=>true);
        if($this->request->is('post')){
            $mId = $this->getCookie('mId');
            $data = $this->request->data;
            $tutar = str_replace(',','.',$data['tutar']);
			$m = $data['m'];
			$i = $data['i'];
			if($mId != $m){
				$tt = $this->SThreeU->findByMidAndId($mId,$i);
				$pp = $this->SThreeU->findByMidAndId($m,$i);
				if(!empty($tt)){
					$this->setCookie('mId',$mId);
					$m = $mId;
				}else if(!empty($pp)){
					$this->setCookie('mId',$m);
					$mId = $m;
				}else{
					echo json_encode($return);
					exit();
				}
			}
			
            $bilgi = $this->SThree->find('first',array('conditions'=>array('cevaplayan_mid'=>$mId, 'cevaplayan_tarih IS NULL','asama'=>2)));
			if($bilgi["SThree"]["oyun"] == 1){
				$tutar = 27;
			}
            $saveData = array('cevaplayan_tutar'=>$tutar,'cevaplayan_tarih'=>date('Y-m-d H:i:s'),'asama'=>3);
            $this->SThree->id = $bilgi['SThree']['id'];
            if($this->SThree->save($saveData)){
                $return['hata'] = false;

            }
        }
        echo json_encode($return);
    }
	
    function OyunSuresiDoldu(){
        $this->autoRender = false;
		$return = array('hata'=>true);
        if($this->request->is('post')){
            $mId = $this->getCookie('mId');
            $data = $this->request->data;
            $asama = $data['asama'];
            $oyun = $data['oyun'];
			
			$url = 'http://'.CURL_URL.'/curlthrees/osdoldu/oyun:'.$oyun.'/asama:'.$asama;
			$curl = curl_init();                
			curl_setopt($curl, CURLOPT_URL, $url);
			curl_setopt($curl,  CURLOPT_RETURNTRANSFER, false);
			curl_exec($curl);   
			curl_close($curl);  
        }
        echo json_encode($return);
    }

    private function RandOyuncuArray($oyuncular){
        $this->autoRender = false;
        $this->RandOyuncuArr = array();
        //$randArray = array();
        shuffle($oyuncular);
        $gonderen = array_slice($oyuncular,0,$this->OyuncuSayisi/2);
        $cevaplayan = array_slice($oyuncular,$this->OyuncuSayisi/2,$this->OyuncuSayisi/2);
        foreach($gonderen as $grow){
            $conditions = array(
                "mid NOT IN (SELECT cevaplayan_mid FROM sthree WHERE gonderen_mid = '".$grow."')",
                "mid NOT IN (SELECT cevaplayan_mid FROM sthree WHERE cevaplayan_tarih IS NULL)",
                "mid !='".$grow."'",
                "mid IN ("."'".implode("','",array_values($cevaplayan))."')",
                !empty($this->RandOyuncuArr)?"mid NOT IN ("."'".implode("','",array_values($this->RandOyuncuArr))."')":"1 = 1"
            );
            $BostakiOyuncular = $this->SThreeU->find('list',array('fields'=>array('mid'),'conditions'=>$conditions));
            $bosoyuncular = array_values($BostakiOyuncular);
            $bostaki = $bosoyuncular[rand(0,count($bosoyuncular)-1)];
            unset($cevaplayan[array_search($bostaki,$cevaplayan)]);
            $this->RandOyuncuArr[$grow] = $bostaki;
            if(empty($bosoyuncular)){
                $this->RandOyuncuArray($oyuncular);
            }
        }

        return $this->RandOyuncuArr;
    }

    function HangiAsama(){
        $this->autoRender = false;
        $return = array('hata'=>true);
        if($this->request->is('post')){
            $gonderimAsamasi = $this->SThree->find('first',array('conditions'=>array('gonderen_mid'=>$this->getCookie('mId'),'gonderen_tarih IS NULL')));
            if($gonderimAsamasi){
                $return['hata'] = false;
                $return['asama'] = 1;
                echo json_encode($return);
            }
            $cevapmAsamasi = $this->SThree->find('first',array('conditions'=>array('cevaplayan_mid'=>$this->getCookie('mId'),'cevaplayan_tarih IS NULL')));
            if($cevapmAsamasi){
                $return['hata'] = false;
                $return['asama'] = 2;
                $return['data'] = $cevapmAsamasi;
                echo json_encode($return);
            }

            echo json_encode($return);
        }
    }

    function AsamaBittiMi(){
        $this->autoRender = false;
        $return = array('sonuc'=>false);
        if($this->request->is('post')){
            $asama = $this->request->data('asama');
            $oyun = $this->request->data('oyun');
            $mId = $this->getCookie('mId');
            //$sonOyun = $this->SThreeG->find('first',array('order'=>array('id'=>'DESC')));
            $conditions = array();
            if($asama == 1){
                $pat = $this->SThree->findAllByOyunAndAsama($oyun,1);
                if(empty($pat)){
                    $dat = $this->SThreeG->findByIdAndAsama($oyun,2);
                    if($dat){
                        $return['sonuc'] = true;
                    }else{
                        $this->SThreeG->id = $oyun;
                        $this->SThreeG->save(array('asama'=>2));
                        $return['sonuc'] = true;
                    }
                }
            }else if($asama == 2){
                $pat = $this->SThree->findAllByOyunAndAsama($oyun,2);
                if(empty($pat)){
                    $dat = $this->SThreeG->findByIdAndAsama($oyun,3);
                    if($dat){
                        $return['sonuc'] = true;
                    }else{
                        $this->SThreeG->id = $oyun;
                        $this->SThreeG->save(array('asama'=>3));
                        $return['sonuc'] = true;
                    }
                }
            }else if($asama == 3){
                $dat = $this->SThreeG->findByIdAndAsama($oyun,4);
                if($dat){
                    $return['sonuc'] = true;
                }
                //$conditions['OR'] = array('gonderen_tarih IS NULL','cevaplayan_tarih IS NULL');
            }else if($asama == 4){
                $dat = $this->SThreeG->findByIdAndAsama($oyun,5);
                if($dat){
                    $return['sonuc'] = true;
                }
            }
            //$sonuc = $this->SThree->find('first',array('conditions'=>$conditions));

            /*if((!$sonuc && ($asama == 1 || $asama == 2)) || ($sonuc && $asama == 0)){
                $return['sonuc'] = true;
            }*/
        }
        echo json_encode($return);
        return false;
    }

    function oyunbitti(){
		$named = $this->request->params['named'];
        if($this->checkCookie('mId')){
            $mId = $this->getCookie('mId');
			$m = $named['m'];
            $i = array_key_exists('i',$named)?$named['i']:0;
			if($mId != $m){
				$tt = $this->SThreeU->findByMidAndId($mId,$i);
				$pp = $this->SThreeU->findByMidAndId($m,$i);
				if(!empty($tt)){
					$this->setCookie('mId',$mId);
					$m = $mId;
				}else if(!empty($pp)){
					$this->setCookie('mId',$m);
					$mId = $m;
				}else{
					$this->redirect(array('controller'=>'surveythrees','action'=>'index'));
				}
			}
            $userBilgi = $this->User->findByMid($mId);
            if($userBilgi){
                $this->set('hesTop',$userBilgi['User']['odenecek']);
                $this->set('code',$userBilgi['User']['code']);
//                $qq = $this->Question->findByMid($mId);
//                if($qq){
//                    $this->set('code',$userBilgi['SThreeU']['code']);
//                }else{
//                    $this->redirect(array('controller'=>'surveythrees','action'=>'question'));
//                }
            }else{
                $this->redirect(array('controller'=>'pages','action'=>'home'));
            }
        }
    }

    function sessifirla(){
        $this->autoRender = false;
        $this->destroyCookie();
        $this->redirect(array('controller'=>'sayfas'));
    }

    function question(){
		$named = $this->request->params['named'];
        if($this->checkCookie('mId')){
            $mId = $this->getCookie('mId');
			$m = $named['m'];
			$i = $named['i'];
			if($mId != $m){
				$tt = $this->SThreeU->findByMidAndId($mId,$i);
				$pp = $this->SThreeU->findByMidAndId($m,$i);
				if(!empty($tt)){
					$this->setCookie('mId',$mId);
					$m = $mId;
				}else if(!empty($pp)){
					$this->setCookie('mId',$m);
					$mId = $m;
				}else{
					$this->redirect(array('controller'=>'surveythrees','action'=>'index'));
				}
			}
			$this->set('UserM',$m);
			$this->set('UserI',$i);
			// $hes = $this->SThree->query("SELECT sum(cevaplayan_tutar) as TOP_GELEN FROM sthree WHERE gdurum = 1 AND gonderen_mid = '".$mId."'");
			// $hes2 = $this->SThree->query("SELECT sum((gonderen_tutar*3)-cevaplayan_tutar) as TOP_GELEN FROM sthree WHERE cdurum = 1 AND cevaplayan_mid = '".$mId."'");
			$hes = $this->SThree->query("SELECT case when sum(gonderen_tutar*3) is null THEN 0 else sum(gonderen_tutar*3) end as TOP_GELEN FROM sthree WHERE gdurum = 1 AND gonderen_mid = '".$mId."'");
			$hes2 = $this->SThree->query("SELECT case when sum(cevaplayan_tutar) is null then 0 else sum(cevaplayan_tutar) end as TOP_GELEN FROM sthree WHERE cdurum = 1 AND cevaplayan_mid = '".$mId."'");
			$hesTop = (($hes[0][0]['TOP_GELEN'] + $hes2[0][0]['TOP_GELEN'])/300)*0.2;
			$this->set('hesTop',$hesTop);
			/* odenecek para miktari isle*/
			$user = $this->User->findByMid($mId);
			$this->User->id = $user['User']['id'];
			$this->User->save(array('odenecek'=>$hesTop));
			/* odenecek para miktari isle son*/
            $userBilgi = $this->SThreeU->findByMid($mId);
            if($userBilgi){
//                $qq = $this->Question->findByMid($mId);
//                if($qq){
//                    $this->redirect(array('controller'=>'surveythrees','action'=>'oyunbitti','m'=>$mId,'i'=>$i));
//                }
                $pata = $this->SThreeU->find('all');
                foreach ($pata as $row){
                    $taka = $this->User->findByMid($row['SThreeU']['mid']);
                    $this->User->id = $taka['User']['id'];
                    $this->User->save(array('durum'=>2));
                }
                $this->redirect(array('controller'=>'surveythrees','action'=>'oyunbitti','m'=>$mId,'i'=>$i));
            }else{
                $this->redirect(array('controller'=>'pages','action'=>'home'));
            }
        }else{
            $this->redirect(array('controller'=>'pages','action'=>'home'));
        }
    }

    function questioncevap(){
        $this->autoRender = false;
        $return = array('hata'=>true,'sonuc'=>0);
        if($this->request->is('post')){
            $dat = $this->request->data;
            if(!$this->checkCookie('mId')){
                // sayfadan at
                $return['sonuc'] = 3;
            }else{
				$mId = $this->getCookie('mId');
				$m = $dat['m'];
				$i = $dat['i'];
				if($mId != $m){
					$tt = $this->SThreeU->findByMidAndId($mId,$i);
					$pp = $this->SThreeU->findByMidAndId($m,$i);
					if(!empty($tt)){
						$this->setCookie('mId',$mId);
						$m = $mId;
					}else if(!empty($pp)){
						$this->setCookie('mId',$m);
						$mId = $m;
					}else{
						$return['sonuc'] = 3;
					}
				}
				$return['m'] = $m;
				$return['i'] = $i;
                $userBilgi = $this->SThreeU->findByMid($mId);
                if($userBilgi && $return['sonuc'] == 0){
                    $qq = $this->Question->findByMid($mId);
                    if($qq){
                        // sonraki aşamaya at
                        $return['sonuc'] = 1;
                        $return['hata'] = false;
                    }else{
                        $dat['mid'] = $mId;
                        $dat['oyun'] = 3;
						unset($dat['m']);
						unset($dat['i']);
                        $this->Question->create();
                        if($this->Question->save($dat)){
                            // sonraki aşamaya at
                            $return['sonuc'] = 1;
                            $return['hata'] = false;
                        }else{
                            $return['sonuc'] = 2;
                        }
                    }
                }else{
                    $return['sonuc'] = 3;
                }
            }
        }

        echo json_encode($return);
    }

    public function indexsurekaydet(){
        $this->autoRender = false;
        if($this->request->is('post')){
            $time = $this->request->data('time');
            $mId = $this->getCookie('mId');
            $user = $this->User->findByMid($mId);
            $this->User->id = $user['User']['id'];
            if($this->User->save(array('zaman'=>$time))){
                echo json_encode(true);
            }else{
                echo json_encode(false);
            }
        }else{
            echo json_encode(false);
        }
    }
	
	private function urloyunac($oyun=1){
		$this->autoRender = false;
        $this->autoLayout = false;
		
		$url = 'http://'.CURL_URL.'/curlthrees/YeniOyunAc/oyun:'.$oyun;
		$curl = curl_init();                
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl,  CURLOPT_RETURNTRANSFER, false);
		curl_exec($curl);   
		curl_close($curl);

        return false;
	}

    public function OyunuBaslat(){
        $this->autoRender = false;
        $this->autoLayout = false;
        if($this->request->is('post')){
            $data = $this->request->data;
            $midVarmi = $this->SThreeU->findByIdAndMidAndDurum($data['i'],$data['m'],1);
            if($midVarmi){
                $this->User->deleteAll(array("mid IS NULL"),true);
                $pata = $this->User->findAllByDurumAndSurvey(0,3);
                foreach($pata as $row){
                    $oyuncusayisi = $this->SThreeU->find('count');
                    $oyuncukayitlimi = $this->SThreeU->findByMid($row['User']['mid']);
                    if(empty($oyuncukayitlimi) && $oyuncusayisi < $this->OyuncuSayisi){
                        $link = 0;
                        $linksifir = $this->SThreeU->find('count',array('conditions'=>array('link'=>0)));
                        if($linksifir >= $this->OyuncuSayisi/2){
                            $link = 1;
                        }
                        $code = uniqid('2');
                        $saved = array('mid'=>$row['User']['mid'], 'ip'=> $this->request->clientIp(), 'code'=>$code, 'link'=>$link, 'tarih'=>date('Y-m-d H:i:s'), 'durum'=>1);
                        $this->SThreeU->create();
                        if($this->SThreeU->save($saved)) {
                            $id = $this->SThreeU->getLastInsertID();
                            $this->SThreeU->save(array('userid' => $id));
                            $user = $this->User->findByMid($row['User']['mid']);
                            $this->User->id = $user['User']['id'];
                            $this->User->save(array('durum' => 1, 'zaman' => 1, 'survey' => 3, 'code' => $code, 'ip' => $this->request->clientIp()));
                        }
                    }
                }
				
				$pata = $this->User->findAllByDurumAndSurvey(-1,3);
                foreach($pata as $row){
                    $oyuncusayisi = $this->SThreeU->find('count');
                    $oyuncukayitlimi = $this->SThreeU->findByMid($row['User']['mid']);
                    if(empty($oyuncukayitlimi) && $oyuncusayisi < $this->OyuncuSayisi){
                        $link = 0;
                        $linksifir = $this->SThreeU->find('count',array('conditions'=>array('link'=>0)));
                        if($linksifir >= $this->OyuncuSayisi/2){
                            $link = 1;
                        }
                        $code = uniqid('2');
                        $saved = array('mid'=>$row['User']['mid'], 'ip'=> $this->request->clientIp(), 'code'=>$code, 'link'=>$link, 'tarih'=>date('Y-m-d H:i:s'), 'durum'=>1);
                        $this->SThreeU->create();
                        if($this->SThreeU->save($saved)) {
                            $id = $this->SThreeU->getLastInsertID();
                            $this->SThreeU->save(array('userid' => $id));
                            $user = $this->User->findByMid($row['User']['mid']);
                            $this->User->id = $user['User']['id'];
                            $this->User->save(array('durum' => 1, 'zaman' => 1, 'survey' => 3, 'code' => $code, 'ip' => $this->request->clientIp()));
                        }
                    }
                }
            }
        }
        echo json_encode(true);
    }
}