<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 * @var $this View
 * @var $html HtmlHelper
 */

$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		Game
	</title>
	<?php
		echo $this->Html->meta('icon');

	echo $this->Html->css('bootstrap.min');
	echo $this->Html->css('mine');
	echo $this->Html->css('font-awesome.min');
	echo $this->Html->css('TimeCircles');
	echo $this->Html->css('jquery-confirm.min');
	echo $this->Html->script('jquery');
	echo $this->Html->script('bootstrap.min');
	echo $this->Html->script('blockUI');
	echo $this->Html->script('TimeCircles/TimeCircles');
	echo $this->Html->script('jquery.mask');
	echo $this->Html->script('jquery-confirm.min');

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
</head>
<body style="padding-top:20px;">
	<?php echo $this->Flash->render(); ?>
	<div class="maincontent">
		<?php echo $this->fetch('content'); ?>
	</div>
	<div class="container text-right" style="margin-bottom: 20px;">
		<img src="<?php echo $this->webroot;?>img/essex.jpg" style="height: 50px;margin-top:30px;" />
	<?php //echo $this->element('sql_dump'); ?>
	</div>


	<audio id="acilissound" controls autoplay style="display: none" src="<?php echo $this->webroot; ?>mp3/acilis.mp3"></audio>
	<script type="text/javascript">
		$(document).ready(function() {
			$('.maincontent').css('min-height', $(window).height() * 0.8);
		});
	</script>
<?php echo date('d.m.Y H:i:s'); ?>
</body>
</html>
