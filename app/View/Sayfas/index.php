<?php
if(!empty($user)){
?>
    <div class="container">
        <h3 class="text-center">You can't play this game.</h3>
        <div class="text-center">
            <h4>Please Wait...</h4>
        </div>
    </div>
    <script type="text/javascript">
        setTimeout(function(){window.location.reload();},30000);
    </script>
<?php
} else {
?>
<div class="container">
    <h3 class="text-center">Not Yet!</h3>
    <div class="text-center">
        <h4>Please Wait...</h4>
    </div>
</div>
<script type="text/javascript">
    setTimeout(function(){window.location.reload();},30000);
</script>
<?php
}
?>