<?php
/**
 * @var $this View
 * @var $html HtmlHelper
 */
?>
<div class="container">
    <div class="row">
        <div class="col-xs-6"><h5>ROUND: <?php echo $game; ?>/10</h5></div>
        <div class="col-xs-6"><?php echo $this->element('CevapSuresi');?></div>
    </div>
	<div class="row">
        <div class="col-xs-6"><h5>Your USER ID: <?php echo $mUser['SOneU']['id']; ?></h5></div>
    </div>
</div>

<?php
if($asama == 1 && $bekle == 0){
    ?>
    <div class="container" id="gonderilecek">
        <div class="row">
            <div class="col-xs-12">
                <form class="form-horizontal" style="margin-top: 10px;">
                    <div class="form-group">
                        <div class="col-xs-12 text-center">
                            <h5>*** Stage 1 ***</h5>
                        </div>
                        <div class="col-xs-12 text-center">
                            <h6>You are playing as First Mover</h6>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-6 text-right"><h6>Your endowment:</h6></label>
                        <div class="col-xs-6">
                            <h6>100</h6>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="gonderilentutar" class="col-xs-6 text-right"><h6>Your offer:</h6></label>
                        <div class="col-xs-6">
                            <input type="text" class="form-control money" name="gonderilentutar" placeholder="TYPE HERE">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12 text-center">
                            <button type="button" class="btn btn-primary" onclick="FuncGonder()">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <?php
} else if($asama == 2 && $bekle == 0) {
    ?>
    <div class="container" id="cevaplanacak">
        <div class="row">
            <div class="col-xs-12">
                <form class="form-horizontal" style="margin-top: 10px;">
                    <div class="form-group">
                        <div class="col-xs-12 text-center">
                            <h5>*** Stage 1 ***</h5>
                        </div>
                        <div class="col-xs-12 text-center">
                            <h5 class="text-danger">You are playing as Second Mover</h5>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-6 text-right">
                            <h6>Your received:</h6>
                        </div>
                        <div class="col-xs-6">
                            <h6><?php echo $cevapmAsamasi['SOne']['gonderen_tutar']*3; ?></h6>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="gonderilentutar" class="col-xs-6 text-right"><h6>Your return:</h6></label>
                        <div class="col-xs-6">
                            <input type="text" class="form-control money" name="kabuledilentutar" placeholder="TYPE HERE">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-12 text-center">
                            <button type="button" class="btn btn-primary" onclick="FuncCevapla()">Submit</button>
                        </div>
                    </div>
                    <input type="hidden" name="gelentutar" value="<?php echo $cevapmAsamasi['SOne']['gonderen_tutar']*3; ?>">
                </form>
            </div>
        </div>
    </div>
    <?php
}else if($asama == 3 && $bekle == 0){
    if(!empty($gonderimAsamasi)){
        ?>
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <form class="form-horizontal" style="margin-top: 10px;">
                        <div class="form-group">
                            <div class="col-lg-12 text-center">
                                <h5>*** Stage 2 ***</h5>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-12 text-center">
                                <h6>Round Summary</h6>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-6 text-right">
                                <h6>Endowment</h6>
                            </div>
                            <div class="col-xs-6">
                                <h6>100</h6>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-6 text-right">
                                <h6>How much you gave</h6>
                            </div>
                            <div class="col-xs-6">
                                <h6><?php echo $gonderimAsamasi['SOne']['gonderen_tutar']; ?></h6>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-6 text-right">
                                <h6>How much you received back</h6>
                            </div>
                            <div class="col-xs-6">
                                <h6><?php echo $gonderimAsamasi['SOne']['cevaplayan_tutar']; ?></h6>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-6 text-right">
                                <h6>Profit</h6>
                            </div>
                            <div class="col-xs-6">
                                <h6><?php echo 100-$gonderimAsamasi['SOne']['gonderen_tutar']+$gonderimAsamasi['SOne']['cevaplayan_tutar']; ?></h6>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <?php
    }else if(!empty($cevapmAsamasi)){
        ?>
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <form class="form-horizontal" style="margin-top: 10px;">
                        <div class="form-group">
                            <div class="col-lg-12 text-center">
                                <h5>*** Stage 2 ***</h5>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-12 text-center">
                                <h6>Round Summary</h6>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-6 text-right">
                                <h6>How much you received</h6>
                            </div>
                            <div class="col-xs-6">
                                <h6><?php echo $cevapmAsamasi['SOne']['gonderen_tutar']*3; ?></h6>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-6 text-right">
                                <h6>How much you returned</h6>
                            </div>
                            <div class="col-xs-6">
                                <h6><?php echo $cevapmAsamasi['SOne']['cevaplayan_tutar']; ?></h6>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-6 text-right">
                                <h6>Profit</h6>
                            </div>
                            <div class="col-xs-6">
                                <h6><?php echo ($cevapmAsamasi['SOne']['gonderen_tutar']*3)-$cevapmAsamasi['SOne']['cevaplayan_tutar']; ?></h6>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <?php
    }
}
echo $this->element('TimeCircles');
echo $this->element('GonderiBasarisiz');
?>
<div id="ekstra" style="display: none;"> 
<?php 
if(!empty($gonderimAsamasi)){
	echo '<p>In This Round You Are Playing As First Mover</p>';
	if($asama == 1){
		echo '<p>Waiting For Other Player</p>';
	}else if($asama == 2){
		echo '<p>Waiting For Second Mover</p>';
	}
}else{
	echo '<p>In This Round You Are Playing As Second Mover</p>';
	if($asama == 1){
		echo '<p>Waiting For First Mover</p>';
	}else if($asama == 2){
		echo '<p>Waiting For Other Player</p>';
	}
}
?>
</div> 
<script type="text/javascript">
    var OyunSuresi = 21000;
    if(<?php echo $asama;?> == 1){
        OyunSuresi = 21000;
    }else if(<?php echo $asama;?> == 2){
        OyunSuresi = 21000;
    }else if(<?php echo $asama;?> == 3){
        OyunSuresi = 11000;
    }
    var gelentutar = 0;
    var mobileCss = 0;

    setInterval(function(){FuncAsamaBittiMi(<?php echo $asama;?>, <?php echo $game; ?>);},2100);
    FuncOyunSuresiBaslat(<?php echo $asama;?>,OyunSuresi,<?php echo $game; ?>);

    $(document).ready(function(){
        $("div#CevapSuresi").data('timer', OyunSuresi/1000);
        $("div#CountDownTimer").data('timer', OyunSuresi/1000);
        $("#CevapSuresi").TimeCircles({ time: { Days: { show: false }, Hours: { show: false }, Minutes: { show: false } }}).addListener(function(unit, amount, total){
            if(total == 0) {
                $("#CevapSuresi").TimeCircles().stop();
            }
        });
        $("#CountDownTimer")
            .TimeCircles({ time: { Days: { show: false }, Hours: { show: false }, Minutes: { show: false } }})
            .addListener(function(unit, amount, total){
                if(total < 7 && total%2 == 0){
                    $('audio#acilissound')[0].play();
                }
                if(total == 0) {
                    $("#CountDownTimer").TimeCircles().stop();
                }
            });

        if($(window).width() < 768){
            mobileCss = 1;
        }
        if(<?php echo $bekle; ?>){
            TimeCirleShow();
            //$.blockUI();
        }

        $('.money').mask("000");
    });

    function FuncHangiAsama(){
        $.ajax({
            async:false,
            type:'POST',
            url:'<?php echo $this->Html->url('/');?>surveys/HangiAsama'
        }).done(function(data){
            var dat = $.parseJSON(data);
            if(dat['hata']){
                window.location.reload();
            }else{
                if(dat['asama'] == 1){

                }else{
                    // Gonderim aşaması açıksa gizle
                    if(!$('#gonderilecek').hasClass('hidden')){
                        $('#gonderilecek').addClass('hidden');
                    }
                    // Cevap aşaması kapalıysa aç
                    if($('#cevaplanacak').hasClass('hidden')){
                        $('#cevaplanacak').removeClass('hidden');
                    }

                    gelentutar = dat['data']['SurveyOne']['gonderen_tutar']*3;
                    $('#cevaplanacak input[name="gId"]').val(dat['data']['SurveyOne']['gonderen_mid']);
                    $('#cevaplanacak input[name="gelentutar"]').val(gelentutar);
                    $('#cevaplanacak input[id="gelentutar"]').val(gelentutar);
                }
            }
        }).fail(function(){
            window.location.reload();
        });
        return false;
    }

    function FuncGonder(){
        var tutar = $('#gonderilecek form input[name="gonderilentutar"]').val();
        var anatutar = 100;
        if(parseInt(tutar) > anatutar || tutar == ''){
            $.blockUI({ message: '<h3 class="text-danger text-center">Fail</h3><h4 class="text-center">You are only able to type a number between 0 and 100.</h4>',onOverlayClick: $.unblockUI  });
        }else{
            $.ajax({
                async:false,
                type:'POST',
                url:'<?php echo $this->Html->url('/');?>surveys/ParaGonder',
                data:'tutar='+tutar+'&i=<?php echo $mUser['SOneU']['id']; ?>&m=<?php echo $mUser['SOneU']['mid']; ?>'
            }).done(function(data){
                var dat = $.parseJSON(data);
                if(dat['hata']){
                    //FuncParaGonderHata();
                    $.blockUI({
                        message: $('#GonderiBasarisiz'),
                        onOverlayClick: $.unblockUI
                    });
                }else{
                    TimeCirleShow();
                }
            }).fail(function(){
                TimeCirleShow();
            });
        }
    }
    function FuncCevapla(){
        var tutar = $('#cevaplanacak form input[name="kabuledilentutar"]').val();
        var anatutar = $('#cevaplanacak form input[name="gelentutar"]').val();

        if(parseInt(tutar) > parseInt(anatutar) || tutar == ''){
            $.blockUI({ message: '<h3 class="text-danger text-center">Fail</h3><h4 class="text-center">You are only able to type a number between 0 and '+anatutar+'!</h4>',onOverlayClick: $.unblockUI  });
        }else{
            $.ajax({
                async:false,
                type:'POST',
                url:'<?php echo $this->Html->url('/');?>surveys/ParaKabulEt',
                data:'tutar='+tutar+'&i=<?php echo $mUser['SOneU']['id']; ?>&m=<?php echo $mUser['SOneU']['mid']; ?>'
            }).done(function(data){
                var dat = $.parseJSON(data);
                if(dat['hata']){
                    //FuncParaKabulEtHata();
                    $.blockUI({
                        message: $('#GonderiBasarisiz'),
                        onOverlayClick: $.unblockUI
                    });
                }else{
                    TimeCirleShow();
                }
            }).fail(function(){
                TimeCirleShow();
            });
        }
    }

    function FuncTumGonderimTamamMi(){
        $.ajax({
            async:false,
            type:'POST',
            url:'<?php echo $this->Html->url('/');?>surveys/TumGonderimTamamMi'
        }).done(function(data){
            var dat = $.parseJSON(data);
            // dat['tamam'] == true; ise cevap aşamasına geç.
            if(dat['tamam']){

            }
        }).fail(function(){

        });
    }
    function FuncTumCevapTamamMi(){
        $.ajax({
            async:false,
            type:'POST',
            url:'<?php echo $this->Html->url('/');?>surveys/TumCevapTamamMi'
        }).done(function(data){
            var dat = $.parseJSON(data);
            // dat['tamam'] == true; ise gonderim aşamasına tekrar gel.
            if(dat['tamam']){

            }
        }).fail(function(){

        });
    }

    function FuncOyunSuresiBaslat(asama,sure,oyun){
        setTimeout(function(){
            $.ajax({
                async:false,
                type:'POST',
                url:'<?php echo $this->Html->url('/');?>surveys/OyunSuresiDoldu',
                data: 'asama='+asama+'&oyun='+oyun
            }).done(function(data){
                var dat = $.parseJSON(data);
                if(dat['hata']){
                    return false;
                }else{
                    if(dat['sonuc'] == 1){
                        window.location.reload();
                    }else{
                        return false;
                    }
                }
            }).fail(function(){
                return false;
            });
        }, sure);
    }

    function FuncAsamaBittiMi(asama,oyun){
        $.ajax({
            async:false,
            type: "POST",
            url: "<?php echo $this->Html->url('/')?>surveys/AsamaBittiMi",
            data: "asama="+asama+"&oyun="+oyun
        }).done(function(data){
            var dat = $.parseJSON(data);
            if(dat['sonuc']){
                window.location.reload();
            }
        }).fail(function(){
            window.location.reload();
        });
    }

    function TimeCirleShow(){
        /*if(mobileCss){
         $.blockUI({ message: $('#TimeCircles'),css: {
         padding:	0,
         margin:		0,
         width:		'90%',
         top:		'20%',
         left:		'5%',
         textAlign:	'center',
         color:		'#000',
         border:		'3px solid #aaa',
         backgroundColor:'#fff',
         cursor:		'wait',
         'border-radius':		'10px'
         } });
         }else{
         $.blockUI({ message: $('#TimeCircles') });
         }*/
		$('#TimeCircles').append($('#ekstra').html());
        $.blockUI({ message: $('#TimeCircles') });
    }
</script>