<?php
/**
 * @property SOneU $SOneU
 * @property SOne $SOne
 * @property SOneG $SOneG
 * @property Question $Question
 * @property User $User
 */
class SurveysController extends AppController{
    public $name = "Surveys";
    public $uses = array('SOneU','SOne','SOneG','Question','User');
    public $components = array('PhpExcel');
    public $helpers = array('PhpExcel');

    public $RandOyuncuArr;
    public $OyuncuSayisi = 10;
    public $Round = 10;

    public function index(){
        $named = $this->request->params['named'];
        if(!$this->Cookie->check('mId') || ($this->Cookie->check('mId') && array_key_exists('mId',$named) && !empty($named['mId']))){
            if(array_key_exists('mId',$named) && !empty($named['mId'])){
                $this->Cookie->destroy();
                $this->Cookie->write('mId',$named['mId']);
            }else{
                $this->redirect(array('controller'=>'pages','action'=>'home'));
            }
        }

        $mId = $this->Cookie->read('mId');
        // mId daha önce sisteme kaydedilmiş mi
        $user = $this->User->findByMid($mId);
        $mIdVarmi = $this->SOneU->findByMid($mId);
        $oyundaMi = $this->SOne->find('all',array('conditions'=>array('OR'=>array('gonderen_mid'=>$mId,'cevaplayan_mid'=>$mId))));

        // Katılımcı sayısı tamam mı?
        $katilimciSayisi = $this->SOneU->find('count');

        if(empty($user)){
            /*if($katilimciSayisi == $this->OyuncuSayisi){
                $this->redirect(array('controller'=>'pages','action'=>'home'));
            }else{
                $this->User->create();
                if($this->User->save(array('mid'=>$mId))){
                    $user = $this->User->findByMid($mId);
                }else{
                    $this->redirect(array('controller'=>'surveys','action'=>'index'));
                }
            }*/
			$this->User->create();
                if($this->User->save(array('mid'=>$mId))){
                    $user = $this->User->findByMid($mId);
                }else{
                    $this->redirect(array('controller'=>'surveys','action'=>'index'));
                }
        }
		/*else if(!empty($user) && empty($mIdVarmi)){
            $this->redirect(array('controller'=>'pages','action'=>'home'));
        }*/
		else if(!empty($oyundaMi)){
            $this->redirect(array('controller'=>'surveys','action'=>'surveyone', 'm'=>$mId, 'i'=>$mIdVarmi['SOneU']['id']));
        }

        $this->set('user',$user);
		$this->set('mIdVarmi',$mIdVarmi);
    }
	
	function OyuncuTamamMi(){
        $this->autoRender = false;
        $return = array('hata'=>true, 'asama'=>0);
		$mId = $this->Cookie->read('mId');
		$user = $this->User->findByMid($mId);
		$userOyundaMi = $this->SOneU->findByMid($mId);
		
        $oyuncusayisi = $this->SOneU->find('count');

        $userAyniOynamismi = $this->User->find('all',array('conditions'=>array('mid'=>$mId,'survey'=>1,'durum'=>1)));
        $userBaskaOyundaOynamismi = $this->User->find('all',array('conditions'=>array('mid'=>$mId,'survey != 1')));

        if($userBaskaOyundaOynamismi){
            $return['full'] = true;
        }else if($userAyniOynamismi && empty($userOyundaMi)){
            $return['full'] = true;
        }else if($oyuncusayisi == $this->OyuncuSayisi && $userOyundaMi){
            $oyunVarMi = $this->SOneG->find('all');
            if($oyunVarMi){
                $return['hata'] = false;
                $return['asama'] = 1;
            }else{
				$this->urloyunac();
                //$this->YeniOyunAc();
                //$this->LinkAta();
            }
			$return['m'] = $userOyundaMi['SOneU']['mid'];
			$return['i'] = $userOyundaMi['SOneU']['id'];
        }else if($oyuncusayisi == $this->OyuncuSayisi && empty($userOyundaMi)){
			$return['full'] = true;
		}else{
			$this->userkaydet();
		}
		
        echo json_encode($return);
        return false;
    }
	
	private function userkaydet(){
		$this->autoRender = false;
		$mId = $this->Cookie->read('mId');
        $user = $this->User->findByMid($mId);
        // mId daha önce sisteme kaydedilmiş mi
        $mIdVarmi = $this->SOneU->findByMid($mId);
        $oyundaMi = $this->SOne->find('all',array('conditions'=>array('OR'=>array('gonderen_mid'=>$mId,'cevaplayan_mid'=>$mId))));
        // Katılımcı sayısı tamam mı?
        $katilimciSayisi = $this->SOneU->find('count');

        if(empty($mIdVarmi)){
            if($katilimciSayisi == $this->OyuncuSayisi){
                return false;
            }else{
                $link = 0;
                $linksifir = $this->SOneU->find('count',array('conditions'=>array('link'=>0)));
                if($linksifir >= $this->OyuncuSayisi/2){
                    $link = 1;
                }
				$code = uniqid('2');
                $saved = array('mid'=>$mId, 'ip'=> $this->request->clientIp(), 'code'=>$code, 'link'=>$link, 'tarih'=>date('Y-m-d H:i:s'));
                $this->SOneU->create();
                if($this->SOneU->save($saved)){
                    $id = $this->SOneU->getLastInsertID();
                    $this->SOneU->save(array('userid'=>$id));
                    $user = $this->User->findByMid($mId);
					$this->User->id = $user['User']['id'];
					$this->User->save(array('durum'=>1, 'zaman'=>1, 'survey'=>1, 'code'=>$code));
                }else{
                    return false;
                }
            }
        }else if(empty($mIdVarmi)){
            $this->redirect(array('controller'=>'pages','action'=>'home'));
        }else if(!empty($oyundaMi)){
            $this->redirect(array('controller'=>'surveys','action'=>'surveyone'));
        }
	}

    public function surveyone(){
		$named = $this->request->params['named'];
         if(!$this->Cookie->check('mId')){
            $this->redirect(array('controller'=>'pages','action'=>'home'));
        }else if(!array_key_exists('m',$named) || !array_key_exists('i',$named) || empty($named['m']) || empty($named['i'])){
			$this->redirect(array('controller'=>'surveys','action'=>'index'));
		}

        $mId = $this->Cookie->read('mId');
		$m = $named['m'];
		$i = $named['i'];
		if($mId != $m){
			$tt = $this->SOneU->findByMidAndId($mId,$i);
			$pp = $this->SOneU->findByMidAndId($m,$i);
			if(!empty($tt)){
				$this->Cookie->write('mId',$mId);
				$m = $mId;
			}else if(!empty($pp)){
				$this->Cookie->write('mId',$m);
				$mId = $m;
			}else{
				$this->redirect(array('controller'=>'surveys','action'=>'index'));
			}
		}

        // Oyun Bitti Mi
        $sonOyun = $this->SOneG->find('first',array('conditions'=>array('asama != 0'),'order'=>array('id'=>'DESC')));
        if($sonOyun && $sonOyun['SOneG']['id'] >= $this->Round && $sonOyun['SOneG']['asama'] == 5){
            $this->redirect(array('controller'=>'surveys','action'=>'question','m'=>$mId, 'i'=>$i));
        }
        // Oyun Bitti Mi SON

        $mUser = $this->SOneU->findByMid($mId);
        if($mUser){
            $asama = $sonOyun['SOneG']['asama'];
            $bekle = 0;

            $gMi = $this->SOne->find('first',array('conditions'=>array('gonderen_mid'=>$mId,'oyun'=>$sonOyun['SOneG']['id'])));
            $cMi = $this->SOne->find('first',array('conditions'=>array('cevaplayan_mid'=>$mId,'oyun'=>$sonOyun['SOneG']['id'])));

            if($asama == 1){
                if($gMi){
                    if($gMi['SOne']['asama'] == 1){
                        $bekle = 0;
                    }else{
                        $bekle = 1;
                    }
                }else if($cMi){
                    $bekle = 1;
                }
            }else if($asama == 2){
                if($cMi){
                    if($cMi['SOne']['asama'] == 2){
                        $bekle = 0;
                    }else{
                        $bekle = 1;
                    }
                }else if($gMi){
                    $bekle = 1;
                }
            }else if($asama == 3){
                if($gMi){
                    if($gMi['SOne']['asama'] == 3){
                        $bekle = 0;
                    }else{
                        $bekle = 1;
                    }
                }else if($cMi){
                    if($cMi['SOne']['asama'] == 3){
                        $bekle = 0;
                    }else{
                        $bekle = 1;
                    }
                }
            }


            /*if(empty($henuzCevaplanmadi) && empty($henuzCevaplanmadi)){
                $sonuc = $this->YeniOyunAc();
            }*/

            $this->set('gonderimAsamasi',$gMi);
            $this->set('cevapmAsamasi',$cMi);
            $this->set('asama',$asama);
            $this->set('bekle',$bekle);
            $this->set('game',$sonOyun['SOneG']['id']);
			$this->set('mUser',$mUser);
        }else{
			$this->redirect(array('controller'=>'pages','action'=>'home'));
        }
    }

    public function ParaGonder(){
        $this->autoRender = false;
        $return = array('hata'=>true);
        if($this->request->is('post')){
            $mId = $this->Cookie->read('mId');
            $data = $this->request->data;
            $tutar = $data['tutar'];
			$m = $data['m'];
			$i = $data['i'];
			if($mId != $m){
				$tt = $this->SOneU->findByMidAndId($mId,$i);
				$pp = $this->SOneU->findByMidAndId($m,$i);
				if(!empty($tt)){
					$this->Cookie->write('mId',$mId);
				}else if(!empty($pp)){
					$this->Cookie->write('mId',$m);
					$mId = $m;
				}else{
					echo json_encode($return);
					exit();
				}
			}

            $bilgi = $this->SOne->find('first',array('conditions'=>array('gonderen_mid'=>$mId, 'gonderen_tarih IS NULL','asama'=>1)));
			if($bilgi["SOne"]["oyun"] == 1){
				$tutar = 30;
			}
            $saveData = array('gonderen_tutar'=>$tutar,'gonderen_tarih'=>date('Y-m-d H:i:s'),'asama'=>2);
            $this->SOne->id = $bilgi['SOne']['id'];
            if($this->SOne->save($saveData)){
                $return['hata'] = false;
            }
        }
        echo json_encode($return);
    }
    public function ParaKabulEt(){
        $this->autoRender = false;
        $return = array('hata'=>true);
        if($this->request->is('post')){
            $mId = $this->Cookie->read('mId');
            $data = $this->request->data;
            $tutar = str_replace(',','.',$data['tutar']);
			$m = $data['m'];
			$i = $data['i'];
			if($mId != $m){
				$tt = $this->SOneU->findByMidAndId($mId,$i);
				$pp = $this->SOneU->findByMidAndId($m,$i);
				if(!empty($tt)){
					$this->Cookie->write('mId',$mId);
				}else if(!empty($pp)){
					$this->Cookie->write('mId',$m);
					$mId = $m;
				}else{
					echo json_encode($return);
					exit();
				}
			}

            $bilgi = $this->SOne->find('first',array('conditions'=>array('cevaplayan_mid'=>$mId, 'cevaplayan_tarih IS NULL','asama'=>2)));
			if($bilgi["SOne"]["oyun"] == 1){
				$tutar = $bilgi["SOne"]["gonderen_tutar"]*0.3;
			}
            $saveData = array('cevaplayan_tutar'=>$tutar,'cevaplayan_tarih'=>date('Y-m-d H:i:s'),'asama'=>3);
            $this->SOne->id = $bilgi['SOne']['id'];
            if($this->SOne->save($saveData)){
                $return['hata'] = false;

            }
        }
        echo json_encode($return);
    }

    function OyunSuresiDoldu(){
		$this->autoRender = false;
		$return = array('hata'=>true);
        if($this->request->is('post')){
            $mId = $this->Cookie->read('mId');
            $data = $this->request->data;
            $asama = $data['asama'];
            $oyun = $data['oyun'];
			
			$url = 'http://b.kaantunc.com/curlones/osdoldu/oyun:'.$oyun.'/asama:'.$asama;
			$curl = curl_init();                
			curl_setopt($curl, CURLOPT_URL, $url);
			curl_setopt($curl,  CURLOPT_RETURNTRANSFER, false);
			curl_exec($curl);   
			curl_close($curl);  
        }
        echo json_encode($return);
    }

    private function YeniOyunAcYok($oyun){
        $this->autoRender = false;
        $return = array('hata'=>true);
		$named = $this->request->params['named'];
		$oyun = $named['oyun'];
        // Son Açılan oyun hangisi onu al.
        $sonOyun = $this->SOneG->find('first',array('conditions'=>array('asama != 0'),'order'=>array('id'=>'DESC')));
        if($sonOyun && $sonOyun['SOneG']['id'] == $oyun && $sonOyun['SOneG']['asama'] == 3 && $sonOyun['SOneG']['id'] < 10){
            $sonuc = $this->OyunVerileriKaydet($oyun);
            return $sonuc;
        }else if(empty($sonOyun)){
			$sonuc = $this->OyunVerileriKaydet();
			return $sonuc;
		}else{
            $this->SOne->updateAll(array('asama'=>5),array('asama'=>4, 'oyun'=>$oyun));
            $this->SOneG->updateAll(array('asama'=>5),array('asama'=>4, 'id'=>$oyun));
			return false;
        }
    }

    private function OyunVerileriKaydet($sonoyun = 0){
        $this->autoRender = false;
        $this->autoLayout = false;

        $sonOyun = $this->SOneG->find('first',array('conditions'=>array('asama != 0'),'order'=>array('id'=>'DESC')));
        if($sonOyun){
            $acilacakOyunVarMi = $this->SOne->find('all',array('conditions'=>array('oyun'=>$sonOyun['SOneG']['id'])));
            if(empty($acilacakOyunVarMi)){
                return false;
            }
        }

        $this->SOneG->create();
        $this->SOneG->save(array('oyun_tarih'=>date('Y-m-d H:i:s'),'asama'=>0));
        $oyun = $this->SOneG->getLastInsertID();

        // Oyuncuları Al
        $oyuncular = $this->SOneU->find('list',array('fields'=>array('mid')));
        $oyuncuesleri = $this->RandOyuncuArray($oyuncular);
        foreach($oyuncuesleri as $key=>$val){
            // Yeni oyun verilerini kaydet
            $saved = array('gonderen_mid'=>$key,'cevaplayan_mid'=>$val, 'oyun'=>$oyun, 'asama'=>0);
            $this->SOne->create();
            $this->SOne->save($saved);
        }
		
        if($sonoyun != 0){
			$this->SOne->updateAll(array('asama'=>5),array('asama'=>3, 'oyun'=>$sonoyun));
            $this->SOneG->updateAll(array('asama'=>5),array('asama'=>3, 'id'=>$sonoyun));
		}
		$this->SOne->updateAll(array('asama'=>1),array('asama'=>0, 'oyun'=>$oyun));
        $this->SOneG->updateAll(array('asama'=>1),array('asama'=>0, 'id'=>$oyun));
        return true;
    }

    private function RandOyuncuArray($oyuncular){
        $this->autoRender = false;
        $this->RandOyuncuArr = array();
        //$randArray = array();
        shuffle($oyuncular);
        $gonderen = array_slice($oyuncular,0,$this->OyuncuSayisi/2);
        $cevaplayan = array_slice($oyuncular,$this->OyuncuSayisi/2,$this->OyuncuSayisi/2);
        foreach($gonderen as $grow){
            $conditions = array(
                "mid NOT IN (SELECT cevaplayan_mid FROM sone WHERE gonderen_mid = '".$grow."')",
                "mid NOT IN (SELECT cevaplayan_mid FROM sone WHERE cevaplayan_tarih IS NULL)",
                "mid !='".$grow."'",
                "mid IN ("."'".implode("','",array_values($cevaplayan))."')",
                !empty($this->RandOyuncuArr)?"mid NOT IN ("."'".implode("','",array_values($this->RandOyuncuArr))."')":"1 = 1"
            );
            $BostakiOyuncular = $this->SOneU->find('list',array('fields'=>array('mid'),'conditions'=>$conditions));
            $bosoyuncular = array_values($BostakiOyuncular);
            $bostaki = $bosoyuncular[rand(0,count($bosoyuncular)-1)];
            unset($cevaplayan[array_search($bostaki,$cevaplayan)]);
            $this->RandOyuncuArr[$grow] = $bostaki;
            if(empty($bosoyuncular)){
                $this->RandOyuncuArray($oyuncular);
            }
        }

        return $this->RandOyuncuArr;
    }

    function HangiAsama(){
        $this->autoRender = false;
        $return = array('hata'=>true);
        if($this->request->is('post')){
            $gonderimAsamasi = $this->SOne->find('first',array('conditions'=>array('gonderen_mid'=>$this->Cookie->read('mId'),'gonderen_tarih IS NULL')));
            if($gonderimAsamasi){
                $return['hata'] = false;
                $return['asama'] = 1;
                echo json_encode($return);
            }
            $cevapmAsamasi = $this->SOne->find('first',array('conditions'=>array('cevaplayan_mid'=>$this->Cookie->read('mId'),'cevaplayan_tarih IS NULL')));
            if($cevapmAsamasi){
                $return['hata'] = false;
                $return['asama'] = 2;
                $return['data'] = $cevapmAsamasi;
                echo json_encode($return);
            }

            echo json_encode($return);
        }
    }

    function AsamaBittiMi(){
        $this->autoRender = false;
        $return = array('sonuc'=>false);
        if($this->request->is('post')){
            $asama = $this->request->data('asama');
            $oyun = $this->request->data('oyun');
            $mId = $this->Cookie->read('mId');
            //$sonOyun = $this->SOneG->find('first',array('order'=>array('id'=>'DESC')));
            $conditions = array();
            if($asama == 1){
                $pat = $this->SOne->findAllByOyunAndAsama($oyun,1);
                if(empty($pat)){
                    $dat = $this->SOneG->findByIdAndAsama($oyun,2);
                    if($dat){
                        $return['sonuc'] = true;
                    }else{
                        $this->SOneG->id = $oyun;
                        $this->SOneG->save(array('asama'=>2));
                        $return['sonuc'] = true;
                    }
                }
            }else if($asama == 2){
                $pat = $this->SOne->findAllByOyunAndAsama($oyun,2);
                if(empty($pat)){
                    $dat = $this->SOneG->findByIdAndAsama($oyun,3);
                    if($dat){
                        $return['sonuc'] = true;
                    }else{
                        $this->SOneG->id = $oyun;
                        $this->SOneG->save(array('asama'=>3));
                        $return['sonuc'] = true;
                    }
                }
            }else if($asama == 3){
                $dat = $this->SOneG->findByIdAndAsama($oyun,5);
                if($dat){
                    $return['sonuc'] = true;
                }
                //$conditions['OR'] = array('gonderen_tarih IS NULL','cevaplayan_tarih IS NULL');
            }
        }
        echo json_encode($return);
        return false;
    }

    private function LinkAta(){
        $this->autoRender = false;
        $this->autoLayout = false;
        $oyuncular = $this->SOneU->find('all');
        foreach($oyuncular as $row){
            $diger = $this->SOneU->find('list',array('fields'=>array('mid'),'conditions'=>array('NOT'=>array('mid'=>$row['SOneU']['mid']))));
            shuffle($diger);
            if($row['SOneU']['link'] == 0){
                // Bir directlink ata
                $rand = rand(0,(count($diger)-1));
                $saved = array('mid'=>$row['SOneU']['mid'], 'lid'=>$diger[$rand], 'link'=>1);
                $this->SOneL->create();
                $this->SOneL->save($saved);
            }else{
                // bir directlink ata, bir indirect link ata
                $rand = rand(0,(count($diger)-1));
                $saved = array('mid'=>$row['SOneU']['mid'], 'lid'=>$diger[$rand], 'link'=>1);
                $this->SOneL->create();
                $this->SOneL->save($saved);
                unlink($diger[$rand]);
                // bir indirect link ata
                $rand = rand(0,(count($diger)-1));
                $saved = array('mid'=>$row['SOneU']['mid'], 'lid'=>$diger[$rand], 'link'=>0);
                $this->SOneL->create();
                $this->SOneL->save($saved);
            }
        }
        return true;
    }

    function oyunbitti(){
		$named = $this->request->params['named'];
        if($this->Cookie->check('mId')){
            $mId = $this->Cookie->read('mId');
			$m = $named['m'];
			$i = $named['i'];
			if($mId != $m){
				$tt = $this->SOneU->findByMidAndId($mId,$i);
				$pp = $this->SOneU->findByMidAndId($m,$i);
				if(!empty($tt)){
					$this->Cookie->write('mId',$mId);
					$m = $mId;
				}else if(!empty($pp)){
					$this->Cookie->write('mId',$m);
					$mId = $m;
				}else{
					$this->redirect(array('controller'=>'surveys','action'=>'index'));
				}
			}
            $userBilgi = $this->SOneU->findByMid($mId);
            if($userBilgi){
                $qq = $this->Question->findByMid($mId);
                if($qq){
                    $this->set('code',$userBilgi['SOneU']['code']);
                }else{
                    $this->redirect(array('controller'=>'surveys','action'=>'question'));
                }
            }else{
                $this->redirect(array('controller'=>'pages','action'=>'home'));
            }
        }
    }

    function sessifirla(){
        $this->autoRender = false;
        $this->Cookie->destroy();
        $this->redirect(array('controller'=>'pages','action'=>'home'));
    }

    function question(){
		$named = $this->request->params['named'];
        if($this->Cookie->check('mId')){
            $mId = $this->Cookie->read('mId');
			$m = $named['m'];
			$i = $named['i'];
			if($mId != $m){
				$tt = $this->SOneU->findByMidAndId($mId,$i);
				$pp = $this->SOneU->findByMidAndId($m,$i);
				if(!empty($tt)){
					$this->Cookie->write('mId',$mId);
					$m = $mId;
				}else if(!empty($pp)){
					$this->Cookie->write('mId',$m);
					$mId = $m;
				}else{
					$this->redirect(array('controller'=>'surveys','action'=>'index'));
				}
			}
			$this->set('UserM',$m);
			$this->set('UserI',$i);
			$hes = $this->SOne->query("SELECT sum(cevaplayan_tutar) as TOP_GELEN FROM sone WHERE gdurum = 1 AND gonderen_mid = '".$mId."'");
			$hes2 = $this->SOne->query("SELECT sum((gonderen_tutar*3)-cevaplayan_tutar) as TOP_GELEN FROM sone WHERE cdurum = 1 AND cevaplayan_mid = '".$mId."'");
			$hesTop = (($hes[0][0]['TOP_GELEN'] + $hes2[0][0]['TOP_GELEN'])/100)*0.2;
			$this->set('hesTop',$hesTop);
			/* odenecek para miktari isle*/
			$user = $this->User->findByMid($mId);
			$this->User->id = $user['User']['id'];
			$this->User->save(array('odenecek'=>$hesTop));
			/* odenecek para miktari isle son*/
            $userBilgi = $this->SOneU->findByMid($mId);
            if($userBilgi){
                $qq = $this->Question->findByMid($mId);
                if($qq){
                    $this->redirect(array('controller'=>'surveys','action'=>'oyunbitti','m'=>$mId,'i'=>$i));
                }
            }else{
                $this->redirect(array('controller'=>'pages','action'=>'home'));
            }
        }else{
            $this->redirect(array('controller'=>'pages','action'=>'home'));
        }
    }

    function questioncevap(){
        $this->autoRender = false;
        $return = array('hata'=>true,'sonuc'=>0);
        if($this->request->is('post')){
            $dat = $this->request->data;
            if(!$this->Cookie->check('mId')){
                // sayfadan at
                $return['sonuc'] = 3;
            }else{
				$mId = $this->Cookie->read('mId');
				$m = $dat['m'];
				$i = $dat['i'];
				if($mId != $m){
					$tt = $this->SOneU->findByMidAndId($mId,$i);
					$pp = $this->SOneU->findByMidAndId($m,$i);
					if(!empty($tt)){
						$this->Cookie->write('mId',$mId);
						$m = $mId;
					}else if(!empty($pp)){
						$this->Cookie->write('mId',$m);
						$mId = $m;
					}else{
						$return['sonuc'] = 3;
					}
				}
                $return['m'] = $m;
				$return['i'] = $i;
                $userBilgi = $this->SOneU->findByMid($mId);
                if($userBilgi && $return['sonuc'] == 0){
                    $qq = $this->Question->findByMid($mId);
                    if($qq){
                        // sonraki aşamaya at
                        $return['sonuc'] = 1;
                        $return['hata'] = false;
                    }else{
                        $dat['mid'] = $mId;
                        $dat['oyun'] = 2;
						unset($dat['m']);
						unset($dat['i']);
                        $this->Question->create();
                        if($this->Question->save($dat)){
                            // sonraki aşamaya at
                            $return['sonuc'] = 1;
                            $return['hata'] = false;
                        }else{
                            $return['sonuc'] = 2;
                        }
                    }
                }else{
                    $return['sonuc'] = 3;
                }
            }
        }

        echo json_encode($return);
    }

    public function indexsurekaydet(){
        $this->autoRender = false;
        if($this->request->is('post')){
            $time = $this->request->data('time');
            $mId = $this->Cookie->read('mId');
            $user = $this->User->findByMid($mId);
            $this->User->id = $user['User']['id'];
            if($this->User->save(array('zaman'=>$time))){
                echo json_encode(true);
            }else{
                echo json_encode(false);
            }
        }else{
            echo json_encode(false);
        }
    }
	
	private function urloyunac($oyun=1){
		$this->autoRender = false;
        $this->autoLayout = false;
		
		$url = 'http://b.kaantunc.com/curlones/YeniOyunAc/oyun:'.$oyun;
		$curl = curl_init();                
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl,  CURLOPT_RETURNTRANSFER, false);
		curl_exec($curl);   
		curl_close($curl);  
		
		return $this->redirect(
            array('controller' => 'pages', 'action' => 'home')
        );
	}
}