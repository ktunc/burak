<?php
/**
 * @property SOneU $SOneU
 * @property SOne $SOne
 * @property SOneG $SOneG
 * @property STwoU $STwoU
 * @property STwo $STwo
 * @property STwoG $STwoG
 * @property STwoL $STwoL
 * @property SThreeU $SThreeU
 * @property SThree $SThree
 * @property SThreeG $SThreeG
 * @property SThreeL $SThreeL
 * @property Question $Question
 * @property SOneYedek $SOneYedek
 * @property STwoYedek $STwoYedek
 * @property SThreeYedek $SThreeYedek
 * @property Admin $Admin
 * @property User $User
 */
App::uses('Mysqldump','Vendor');
class AdminsController extends AppController{
    public $name = "Admins";
    public $uses = array('SOneU','SOne','SOneG','SOneYedek',
        'STwoU','STwo','STwoG','STwoL','STwoYedek',
        'SThreeU','SThree','SThreeG','SThreeL','SThreeYedek',
        'Question','Admin','User');
    public $components = array('PhpExcel');
    public $helpers = array('PhpExcel');

    public function test1(){
        $this->autoRender = false;
		
		$url = 'http://burak.kaantunc.com/admins/testekle/oyun:23/asama:2';
		$curl = curl_init();                
		//$post['test'] = 'test';
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt ($curl, CURLOPT_POST, TRUE);
		//curl_setopt ($curl, CURLOPT_POSTFIELDS, $post); 

		curl_setopt($curl, CURLOPT_USERAGENT, 'api');
		curl_setopt($curl, CURLOPT_TIMEOUT, 1);
		curl_setopt($curl, CURLOPT_HEADER, 0);
		curl_setopt($curl,  CURLOPT_RETURNTRANSFER, false);
		curl_setopt($curl, CURLOPT_FORBID_REUSE, true);
		curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 1);
		curl_setopt($curl, CURLOPT_DNS_CACHE_TIMEOUT, 10); 
		curl_setopt($curl, CURLOPT_FRESH_CONNECT, true);
		$data = curl_exec($curl);   
		curl_close($curl);  
		
		/*
		App::uses('HttpSocket', 'Network/Http');
		$HttpSocket = new HttpSocket();
		$results = $HttpSocket->get('http://burak.kaantunc.com/admins/testekle/oyun:23/asama:2');
		*/
		
        /*ob_end_clean();
        header("Connection: close\r\n");
        header("Content-Encoding: none\r\n");
        header("Content-Length: 1");
        ignore_user_abort(true);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_TIMEOUT_MS, 1);
        curl_setopt($ch, CURLOPT_NOSIGNAL, 1);
        //curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL,
            'http://localhost/buraksurvey/admins/testekle/oyun:23/asama:2'
        );
        $content = curl_exec($ch);*/
        //$link = 'wget http://localhost/buraksurvey/admins/testekle/oyun:23/asama:2  > /dev/null &';
        //$link = '/user/kaantunc/php wget http://burak.kaantunc.com/admins/testekle/oyun:23/asama:2  > /dev/null &';
        //$link = 'sh /user/kaantunc/testgit/test.sh > /dev/null &';
        //exec('xdg-open '.$link.' &');
        //system($link);
        //shell_exec($link);
        return $this->redirect(
            array('controller' => 'pages', 'action' => 'home')
        );
    }
    public function testekle(){
        $this->autoRender = false;
        $named = $this->request->params['named'];
        $asama = array_key_exists('asama',$named)?$named['asama']:false;
        $oyun = array_key_exists('oyun',$named)?$named['oyun']:false;
        sleep(10);
        $this->Admin->create();
        $this->Admin->save(array('user'=>$oyun,'pass'=>$asama));
        return $this->redirect(
            array('controller' => 'pages', 'action' => 'home')
        );
    }

    private function dbyedekal($name,$klasor){
        $dbyedekname = $name.'_db_'.date('Ymd_His').'.sql.gz';
        //$dump = new Mysqldump('mysql:host=localhost;dbname=buraksurvey', 'root', '', array('compress' => Mysqldump::GZIP));
		$dump = new Mysqldump('mysql:host=localhost;dbname=kaantunc_burak', 'kaantunc_burak', '123burak456', array('compress' => Mysqldump::GZIP));
        //$dump->start('/home/u6496630/sqlbackup/'.$dbyedekname);
        $dump->start(WWW_ROOT.'sqlbackup/'.$klasor.'/'.$dbyedekname);
        return $dbyedekname;
    }

    public function sone(){
        $datas = $this->SOne->find('all',array('order'=>array('oyun'=>'DESC','id'=>'ASC')));
        $this->set('datas',$datas);
        $yedek = $this->SOneYedek->find('all',array('order'=>array('id'=>'ASC')));
        $this->set('yedek',$yedek);
        $oyuncular = $this->User->findAllBySurvey(1);
        $this->set('oyuncular',$oyuncular);
    }

    public function soneyedekal(){
        $this->autoRender = false;
        if($this->request->is('post')){
            $data = $this->request->data;
            $name = $data['name'];
            $this->yedekalone($name);
            return $this->redirect(
                array('controller' => 'admins', 'action' => 'sone')
            );
        }
    }

    private function yedekalone($name){
        $this->autoRender = false;
        $this->autoLayout = false;

        $oyunname = $this->soneoyunyedek($name);
        $username = $this->soneuseryedek();
        $dbyedekname = $this->dbyedekal($name,'sone');

        $this->SOneYedek->create();
        return $this->SOneYedek->save(array('name'=>$name,'oyun_path'=>$oyunname,'user_path'=>$username,'dbyedek'=>$dbyedekname));
    }

    private function soneoyunyedek($name){
        $filename = $name.'_oyun_'.date('Ymd_His').'.xlsx';
        $datas = $this->SOne->find('all',array('order'=>array('oyun'=>'DESC','id'=>'DESC')));

        $this->PhpExcel->createWorksheet()->setDefaultFont('Calibri', 12);
        $table = array(
            array('label' => 'id'),
            array('label' => 'First Mover', 'filter' => true),
            array('label' => 'Sent Money', 'filter' => true),
            array('label' => 'First Durum', 'filter' => true),
            array('label' => 'Round', 'filter' => true),
            array('label' => 'Second Mover', 'filter' => true),
            array('label' => 'Returned Money', 'filter' => true),
            array('label' => 'Second Durum', 'filter' => true)
        );
        $this->PhpExcel->addTableHeader($table, array('name' => 'Cambria', 'bold' => true));
        foreach($datas as $row){
            $this->PhpExcel->addTableRow(array(
                    $row['SOne']['id'],
                    $row['SOne']['gonderen_mid'],
                    $row['SOne']['gonderen_tutar'],
                    $row['SOne']['gdurum'],
                    $row['SOne']['oyun'],
                    $row['SOne']['cevaplayan_mid'],
                    $row['SOne']['cevaplayan_tutar'],
                    $row['SOne']['cdurum']
                )
            );
        }
        $this->PhpExcel->addTableFooter()->save(WWW_ROOT.'excel/sone/'.$filename);
        return $filename;
    }

    private function soneuseryedek(){
        $filename = 'user_'.date('Ymd_His').'.xlsx';
        $datas = $this->SOneU->find('all',array(
				'fields'=>array('SOneU.tarih','SOneU.userid','SOneU.ip','User.mid','User.code','User.odenecek'),
				'joins'=>array(
					array(
						'table'=>'user',
						'alias'=>'User',
						'type'=>'INNER',
						'conditions'=>array('SOneU.mid = User.mid')
					)
				)
			)
		);

        $this->PhpExcel->createWorksheet()->setDefaultFont('Calibri', 12);
        $table = array(
            array('label' => 'User ID', 'filter' => true),
            array('label' => 'MTurk ID', 'filter' => true),
            array('label' => 'IP', 'filter' => true),
            array('label' => 'Tarih', 'filter' => true),
            array('label' => 'Code', 'filter' => true),
			array('label' => 'Odenecek', 'filter' => true)
        );
        $this->PhpExcel->addTableHeader($table, array('name' => 'Cambria', 'bold' => true));
        foreach($datas as $row){
            $this->PhpExcel->addTableRow(array(
					$row['SOneU']['userid'],
                    $row['User']['mid'],
                    $row['SOneU']['ip'],
                    $row['SOneU']['tarih'],
                    $row['User']['code'],
					$row['User']['odenecek'],
                )
            );
        }
        $this->PhpExcel->addTableFooter()->save(WWW_ROOT.'excel/sone/'.$filename);
        return $filename;
    }

    public function sonesifirla(){
        $this->autoRender = false;
        $return = false;
        if($this->request->is('post')){
            $return = $this->yedekalone('sifirla');
            $this->SOne->query('TRUNCATE TABLE sone');
            $this->SOneG->query('TRUNCATE TABLE soneg');
            $this->SOneU->query('TRUNCATE TABLE soneu');
        }
        echo json_encode($return);
    }

    // STWO Yedek İslemleri

    public function stwo(){
        $datas = $this->STwo->find('all',array('order'=>array('oyun'=>'DESC','id'=>'ASC')));
        $this->set('datas',$datas);
        $yedek = $this->STwoYedek->find('all',array('order'=>array('id'=>'ASC')));
        $this->set('yedek',$yedek);

        $oyuncular = $this->User->findAllBySurvey(2);
        $this->set('oyuncular',$oyuncular);
    }

    public function stwoyedekal(){
        $this->autoRender = false;
        if($this->request->is('post')){
            $data = $this->request->data;
            $name = $data['name'];
            $this->yedekaltwo($name);
            return $this->redirect(
                array('controller' => 'admins', 'action' => 'stwo')
            );
        }
    }

    private function yedekaltwo($name){
        $this->autoRender = false;
        $this->autoLayout = false;

        $oyunname = $this->stwooyunyedek($name);
        $username = $this->stwouseryedek();
        $userlink = $this->stwouserlinkyedek();
        $dbyedekname = $this->dbyedekal($name,'stwo');

        $this->STwoYedek->create();
        return $this->STwoYedek->save(array('name'=>$name,'oyun_path'=>$oyunname, 'user_path'=>$username, 'link_path'=>$userlink, 'dbyedek'=>$dbyedekname));
    }

    private function stwooyunyedek($name){
        $filename = $name.'_oyun_'.date('Ymd_His').'.xlsx';
        $datas = $this->STwo->find('all',array('order'=>array('oyun'=>'DESC','id'=>'DESC')));

        $this->PhpExcel->createWorksheet()->setDefaultFont('Calibri', 12);
        $table = array(
            array('label' => 'id'),
            array('label' => 'First Mover', 'filter' => true),
            array('label' => 'Sent Money', 'filter' => true),
            array('label' => 'Sent Money*3', 'filter' => true),
            array('label' => 'First Durum', 'filter' => true),
            array('label' => 'Round', 'filter' => true),
            array('label' => 'Second Mover', 'filter' => true),
            array('label' => 'Returned Money', 'filter' => true),
            array('label' => 'Second Durum', 'filter' => true)
        );
        $this->PhpExcel->addTableHeader($table, array('name' => 'Cambria', 'bold' => true));
        foreach($datas as $row){
            $this->PhpExcel->addTableRow(array(
                    $row['STwo']['id'],
                    $row['STwo']['gonderen_mid'],
                    $row['STwo']['gonderen_tutar'],
                    $row['STwo']['gonderen_tutar']*3,
                    $row['STwo']['gdurum']==0?'PC':'USER',
                    $row['STwo']['oyun'],
                    $row['STwo']['cevaplayan_mid'],
                    $row['STwo']['cevaplayan_tutar'],
                    $row['STwo']['cdurum']==0?'PC':'USER'
                )
            );
        }
        $this->PhpExcel->addTableFooter()->save(WWW_ROOT.'excel/stwo/'.$filename);
        return $filename;
    }

    private function stwouseryedek(){
        $filename = 'user_'.date('Ymd_His').'.xlsx';
        $datas = $this->STwoU->find('all',array(
				'fields'=>array('STwoU.link','STwoU.tarih','STwoU.userid','STwoU.ip','User.mid','User.code','User.odenecek'),
				'joins'=>array(
					array(
						'table'=>'user',
						'alias'=>'User',
						'type'=>'INNER',
						'conditions'=>array('STwoU.mid = User.mid')
					)
				)
		));

        $this->PhpExcel->createWorksheet()->setDefaultFont('Calibri', 12);
        $table = array(
            array('label' => 'User ID', 'filter' => true),
            array('label' => 'MTurk ID', 'filter' => true),
            array('label' => 'IP', 'filter' => true),
            array('label' => 'Link'),
            array('label' => 'Tarih', 'filter' => true),
            array('label' => 'Code', 'filter' => true),
			array('label' => 'Ödenecek', 'filter' => true)
        );
        $this->PhpExcel->addTableHeader($table, array('name' => 'Cambria', 'bold' => true));
        foreach($datas as $row){
            $link = '1 direct link';
            if($row['STwoU']['link'] == 1){
                $link = '1 direct 1 indirect link';
            }

            $this->PhpExcel->addTableRow(array(
                    $row['STwoU']['userid'],
                    $row['User']['mid'],
                    $row['STwoU']['ip'],
                    $link,
                    $row['STwoU']['tarih'],
                    $row['User']['code'],
					$row['User']['odenecek']
                )
            );
        }
        $this->PhpExcel->addTableFooter()->save(WWW_ROOT.'excel/stwo/'.$filename);
        return $filename;
    }

    private function stwouserlinkyedek(){
        $filename = 'userlink_'.date('Ymd_His').'.xlsx';
        $datas = $this->STwoL->find('all',array('order'=>array('id'=>'ASC')));

        $this->PhpExcel->createWorksheet()->setDefaultFont('Calibri', 12);
        $table = array(
            array('label' => 'id'),
            array('label' => 'MTurk ID', 'filter' => true),
            array('label' => 'Link User MID', 'filter' => true),
            array('label' => 'Link')
        );
        $this->PhpExcel->addTableHeader($table, array('name' => 'Cambria', 'bold' => true));
        foreach($datas as $row){
            $link = 'Direct Link';
            if($row['STwoL']['link'] == 0){
                $link = 'Indirect Link';
            }

            $this->PhpExcel->addTableRow(array(
                    $row['STwoL']['id'],
                    $row['STwoL']['mid'],
                    $row['STwoL']['lid'],
                    $link
                )
            );
        }
        $this->PhpExcel->addTableFooter()->save(WWW_ROOT.'excel/stwo/'.$filename);
        return $filename;
    }

    public function stwosifirla(){
        $this->autoRender = false;
        $return = false;
        if($this->request->is('post')){
            $return = $this->yedekaltwo('sifirla');
            $this->STwo->query('TRUNCATE TABLE stwo');
            $this->STwoG->query('TRUNCATE TABLE stwog');
            $this->STwoU->query('TRUNCATE TABLE stwou');
            $this->STwo->query('TRUNCATE TABLE stwol');
			$this->User->query("UPDATE user SET durum = -1 WHERE mid LIKE 'T1PC%'");
        }
        echo json_encode($return);
    }

    // SThree Yedek İslemleri
    public function sthree(){
        $datas = $this->SThree->find('all',array('order'=>array('oyun'=>'DESC','id'=>'ASC')));
        $this->set('datas',$datas);
        $yedek = $this->SThreeYedek->find('all',array('order'=>array('id'=>'ASC')));
        $this->set('yedek',$yedek);

        $oyuncular = $this->User->findAllBySurvey(3);
        $this->set('oyuncular',$oyuncular);
    }

    public function sthreeyedekal(){
        $this->autoRender = false;
        if($this->request->is('post')){
            $data = $this->request->data;
            $name = $data['name'];
            $this->yedekalthree($name);
            return $this->redirect(
                array('controller' => 'admins', 'action' => 'sthree')
            );
        }
    }

    private function yedekalthree($name){
        $this->autoRender = false;
        $this->autoLayout = false;

        $oyunname = $this->sthreeoyunyedek($name);
        $username = $this->sthreeuseryedek();
        $userlink = $this->sthreeuserlinkyedek();
        $dbyedekname = $this->dbyedekal($name,'sthree');

        $this->SThreeYedek->create();
        return $this->SThreeYedek->save(array('name'=>$name,'oyun_path'=>$oyunname, 'user_path'=>$username, 'link_path'=>$userlink, 'dbyedek'=>$dbyedekname));
    }

    private function sthreeoyunyedek($name){
        $filename = $name.'_oyun_'.date('Ymd_His').'.xlsx';
        $datas = $this->SThree->find('all',array('order'=>array('oyun'=>'DESC','id'=>'DESC')));

        $this->PhpExcel->createWorksheet()->setDefaultFont('Calibri', 12);
        $table = array(
            array('label' => 'id'),
            array('label' => 'First Mover', 'filter' => true),
            array('label' => 'Sent Money', 'filter' => true),
			array('label' => 'Sent Money*3', 'filter' => true),
            array('label' => 'First Durum', 'filter' => true),
            array('label' => 'Round', 'filter' => true),
            array('label' => 'Second Mover', 'filter' => true),
            array('label' => 'Returned Money', 'filter' => true),
            array('label' => 'Second Durum', 'filter' => true)
        );
        $this->PhpExcel->addTableHeader($table, array('name' => 'Cambria', 'bold' => true));
        foreach($datas as $row){
            $this->PhpExcel->addTableRow(array(
                    $row['SThree']['id'],
                    $row['SThree']['gonderen_mid'],
                    $row['SThree']['gonderen_tutar'],
                    $row['SThree']['gonderen_tutar']*3,
                    $row['SThree']['gdurum']==0?'PC':'USER',
                    $row['SThree']['oyun'],
                    $row['SThree']['cevaplayan_mid'],
                    $row['SThree']['cevaplayan_tutar'],
                    $row['SThree']['cdurum']==0?'PC':'USER'
                )
            );
        }
        $this->PhpExcel->addTableFooter()->save(WWW_ROOT.'excel/sthree/'.$filename);
        return $filename;
    }

    private function sthreeuseryedek(){
        $filename = 'user_'.date('Ymd_His').'.xlsx';
        $datas = $this->SThreeU->find('all',array(
				'fields'=>array('SThreeU.link','SThreeU.tarih','SThreeU.userid','SThreeU.ip','User.mid','User.code','User.odenecek'),
				'joins'=>array(
					array(
						'table'=>'user',
						'alias'=>'User',
						'type'=>'INNER',
						'conditions'=>array('SThreeU.mid = User.mid')
					)
				)
		));

        $this->PhpExcel->createWorksheet()->setDefaultFont('Calibri', 12);
        $table = array(
            array('label' => 'User ID', 'filter' => true),
            array('label' => 'MTurk ID', 'filter' => true),
            array('label' => 'IP', 'filter' => true),
            array('label' => 'Link'),
            array('label' => 'Tarih', 'filter' => true),
            array('label' => 'Code', 'filter' => true),
			array('label' => 'Ödenecek', 'filter' => true)
        );
        $this->PhpExcel->addTableHeader($table, array('name' => 'Cambria', 'bold' => true));
        foreach($datas as $row){
            $link = '1 direct link';
            if($row['SThreeU']['link'] == 1){
                $link = '1 direct 5 indirect link';
            }

            $this->PhpExcel->addTableRow(array(
                    $row['SThreeU']['userid'],
                    $row['User']['mid'],
                    $row['SThreeU']['ip'],
                    $link,
                    $row['SThreeU']['tarih'],
                    $row['User']['code'],
					$row['User']['odenecek']
                )
            );
        }
        $this->PhpExcel->addTableFooter()->save(WWW_ROOT.'excel/sthree/'.$filename);
        return $filename;
    }

    private function sthreeuserlinkyedek(){
        $filename = 'userlink_'.date('Ymd_His').'.xlsx';
        $datas = $this->SThreeL->find('all',array('order'=>array('id'=>'ASC')));

        $this->PhpExcel->createWorksheet()->setDefaultFont('Calibri', 12);
        $table = array(
            array('label' => 'id'),
            array('label' => 'MTurk ID', 'filter' => true),
            array('label' => 'Link User MID', 'filter' => true),
            array('label' => 'Link')
        );
        $this->PhpExcel->addTableHeader($table, array('name' => 'Cambria', 'bold' => true));
        foreach($datas as $row){
            $link = 'Direct Link';
            if($row['SThreeL']['link'] == 0){
                $link = 'Indirect Link';
            }

            $this->PhpExcel->addTableRow(array(
                    $row['SThreeL']['id'],
                    $row['SThreeL']['mid'],
                    $row['SThreeL']['lid'],
                    $link
                )
            );
        }
        $this->PhpExcel->addTableFooter()->save(WWW_ROOT.'excel/sthree/'.$filename);
        return $filename;
    }

    public function sthreesifirla(){
        $this->autoRender = false;
        $return = false;
        if($this->request->is('post')){
            $return = $this->yedekalthree('sifirla');
            $this->SThree->query('TRUNCATE TABLE sthree');
            $this->SThreeG->query('TRUNCATE TABLE sthreeg');
            $this->SThreeU->query('TRUNCATE TABLE sthreeu');
            $this->SThree->query('TRUNCATE TABLE sthreel');
            $this->User->query("UPDATE user SET durum = -1 WHERE mid LIKE 'T2PC%'");
        }
        echo json_encode($return);
    }

    public function login(){
        if($this->Session->check('userCheck')){
            return $this->redirect(
                array('controller' => 'admins', 'action' => 'sone')
            );
        }
    }

    public function loginCheck(){
        $this->autoRender = false;
        if($this->request->is('post')){
            if($this->Session->check('userCheck')){
                return $this->redirect(
                    array('controller' => 'admins', 'action' => 'sone')
                );
            }else{
                $data = $this->request->data;
                $username = $data['username'];
                $pass = $data['pass'];
                $pass = Security::hash($pass, 'sha1', true);
                $user = $this->Admin->find('first',array('conditions'=>array('user'=>$username,'pass'=>$pass)));
                if($user){
                    $this->Session->write('user',$user['Admin']['user']);
                    $this->Session->write('userCheck',1);
                    return $this->redirect(
                        array('controller' => 'admins', 'action' => 'sone')
                    );
                }else{
                    return $this->redirect(
                        array('controller' => 'admins', 'action' => 'login')
                    );
                }
            }
        }else{
            return $this->redirect(
                array('controller' => 'admins', 'action' => 'login')
            );
        }
    }

    public function soneoyuncuekle(){
        $this->autoRender = false;
        if($this->request->is('post')){
            $data = $this->request->data;
            $midVarmi = $this->User->findByMid($data['mid']);
            $oyuncusayisi = $this->SOneU->find('count');

            if(empty($midVarmi) && $oyuncusayisi < 12){
                $this->User->create();
                $save = array('mid'=>$data['mid'],'survey'=>1);
                $this->User->save($save);
                $this->SOneU->create();
                $save = array('mid'=>$data['mid']);
                $this->SOneU->save($save);
                return $this->redirect(
                    array('controller' => 'admins', 'action' => 'sone')
                );
            }else{
                return $this->redirect(
                    array('controller' => 'admins', 'action' => 'sone')
                );
            }
        }else{
            return $this->redirect(
                array('controller' => 'admins', 'action' => 'sone')
            );
        }
    }

    public function stwooyuncuekle(){
        $this->autoRender = false;
        if($this->request->is('post')){
            $data = $this->request->data;
            $midVarmi = $this->User->findByMid($data['mid']);

            if(empty($midVarmi)){
                $this->User->create();
                $save = array('mid'=>$data['mid'],'survey'=>2);
                $this->User->save($save);
                return $this->redirect(
                    array('controller' => 'admins', 'action' => 'stwo')
                );
            }else{
                return $this->redirect(
                    array('controller' => 'admins', 'action' => 'stwo')
                );
            }
        }else{
            return $this->redirect(
                array('controller' => 'admins', 'action' => 'stwo')
            );
        }
    }

    public function sthreeoyuncuekle(){
        $this->autoRender = false;
        if($this->request->is('post')){
            $data = $this->request->data;
            $midVarmi = $this->User->findByMid($data['mid']);

            if(empty($midVarmi)){
                $this->User->create();
                $save = array('mid'=>$data['mid'],'survey'=>3);
                $this->User->save($save);
                return $this->redirect(
                    array('controller' => 'admins', 'action' => 'sthree')
                );
            }else{
                return $this->redirect(
                    array('controller' => 'admins', 'action' => 'sthree')
                );
            }
        }else{
            return $this->redirect(
                array('controller' => 'admins', 'action' => 'sthree')
            );
        }
    }
}