<?php
if($mIdVarmi || (!empty($user) && $user['User']['durum'] == 0)){
?>
<div class="container">
    <!--<div id="IndexSuresi" data-timer="180" style="width: 250px; margin-right: auto; margin-left:auto"></div>-->
	<h3 class="text-center">INSTRUCTIONS</h3>
	<div class="divclass firstp">
		<p>Each player will now be assigned a numeric ID that is kept the same throughout the game. ID assignments are random and carry no particular meaning. Each player will also be assigned a certain number of links with other players. Each player will have between 1 and 2 links. </p>
		<p><em>This means that <b>there will be a low average number of links per player.</b></em></p>
		<p>You will play 10 rounds of a "game".</p> 
		<p><em>At the beginning of each round you will be randomly assigned to a role. There are two possible roles in the game: "First Mover" and "Second Mover". Each round is the same.</em></p>
		<p class="text-right"><button type="button" class="btn btn-xs btn-success" onclick="FuncDivAc('secondp')">Next <i class="fa fa-chevron-right"></i></button></p>
	</div>
	<div class="divclass secondp hidden">
		<p>If you are playing as a “First Mover”, in stage 1 you will be given 100 Experimental Points (equivalent to $ 0,20). After, you will be randomly matched with another player whom you will be able to identify by his/her ID.</p> 
		<p><em><b>You will be able to see how the other player behaved in past occasions if he/she played with one of your links as Second Mover.</b></em></p> 
		<p>You can decide to send any amount of Experimental Points to the other person. <em>This amount will be multiplied by 3.</em> The other person will then decide whether to return part of the Experimental Points or not.</p>
		<p><em>Notice that <b>you will play with the same person only once across all rounds.</b></em></p>
		<p>In stage 2, you will visualise a short summary of the round and report your experience to your links. Finally, in stage 3 you will receive reports from your links who played as First Movers. This information will be saved and automatically displayed in future Rounds.</p>
		<p><div class="row">
			<div class="col-xs-6"><button type="button" class="btn btn-xs btn-danger" onclick="FuncDivAc('firstp')"><i class="fa fa-chevron-left"></i> Back</button></div>
			<div class="col-xs-6 text-right"><button type="button" class="btn btn-xs btn-success" onclick="FuncDivAc('thirdp')">Next <i class="fa fa-chevron-right"></i></button></div>
		</div></p>
	</div>
	<div class="divclass thirdp hidden">
		<p>If you are playing as a “Second Mover”, you will receive a certain amount of Experimental Points from a player you have been randomly matched with. The amount originally sent by the other player is multiplied by 3. You can decide to return or not any amount of the Experimental Points to the other person.</p> 
		<p><em>Notice that <b>you will play with the same person only once across all rounds.</b></em></p> 
		<p>In stage 2, you will be able to visualize a short summary of the round. Finally, in stage 3 you will receive reports from your social links who played as First Movers in the current Round. This information will be saved and automatically displayed.</p>
		<p><div class="row">
			<div class="col-xs-6"><button type="button" class="btn btn-xs btn-danger" onclick="FuncDivAc('secondp')"><i class="fa fa-chevron-left"></i> Back</button></div>
			<div class="col-xs-6 text-right"><button type="button" class="btn btn-xs btn-success" onclick="FuncKontrolEt()">Next <i class="fa fa-chevron-right"></i></button></div>
		</div></p>
	</div>
</div>
<div id="bekle" style="display:none"><h4>The round is starting.<br>Please Wait.</h4></div>
<script type="text/javascript">
    var ss = true;
    $(document).ready(function(){
		<?php if($user['User']['durum'] == 1 && !empty($mIdVarmi)){ ?>
		FuncKontrolEt();
		<?php } ?>
    });

//setTimeout(function(){FuncKontrolEt();},180000);

function FuncKontrolEt(){
    $.blockUI({ message: $('#bekle') });
    setInterval(function(){
        $.ajax({
            async:false,
            type: 'POST',
            url: '<?php echo $this->Html->url('/');?>surveytwos/OyuncuTamamMi'
        }).done(function(data){
            var dat = $.parseJSON(data);
            if(dat['hata']){
                if(dat['full']){
                    $.unblockUI();
					// alert('This game is full now.');
					$.alert({
						title: 'Unfortunately, the session is full.',
						content: 'Please contact me at <b>sloiac@essex.ac.uk</b> if you would like to know the time & day of our next session.',
						type: 'red',
						icon: 'fa fa-warning',
						boxWidth:'50%',
						useBootstrap: false,
						buttons: {
							ok:{
								btnClass:'btn-primary',
								action:function(){window.location.href = '<?php echo $this->Html->url('/');?>sayfas/full';}
							}
						}
					});
                }
                if(dat['oyunsuresibaslat'] && ss){
                    OyunSayaciniBaslat(dat['m'],dat['i']);
                }
				$('div#bekle').html('<h4>The round is starting.<br>Please Wait.</h4>');
                //$('div#bekle').html('<h4>The round is starting.<br>Please Wait.</h4>'+dat['mesaj']);
            }else{
                window.location.href = '<?php echo $this->Html->url('/');?>surveytwos/surveytwo/m:'+dat['m']+'/i:'+dat['i'];
            }
        }).fail(function(){
            $.alert('Please try again.');
            return false;
        });
    },1500);
}
function FuncDivAc(div){
    $('.divclass').addClass('hidden');
    $('.'+div).removeClass('hidden');
}

function OyunSayaciniBaslat(m,i){
    setTimeout(function(){
        $.ajax({
            async:false,
            type: 'POST',
            url: '<?php echo $this->Html->url('/');?>surveytwos/OyunuBaslat',
            data:{'m':m,'i':i}
        }).done(function(data){

        }).fail(function(){
             $.alert('Please try again.');
            return false;
        });
    },5000);
    // 240000
    // 180000
}
</script>
<?php
} else {
?>
<div class="container">
        <h3 class="text-center">Unfortunately, we are unable to connect you back to the experiment!</h3>
        <div class="text-center">
            <h4>Please contact me at <em>"sloiac@essex.ac.uk"</em> if you would like to know the time & day of our next session.</h4>
        </div>
    </div>
    <script type="text/javascript">
        setTimeout(function(){window.location.reload();},30000);
    </script>
<?php
}
?>