<div class="container" id="profit">
    <div class="row">
        <div class="col-xs-12 text-center">
            <h2>Your profit (as your bonus payment): $ <?php echo $hesTop; ?></h2>
        </div>
    </div>
	<div class="row">
        <div class="col-xs-12 text-center">
            <button type="button" class="btn btn-success" onclick="FuncNextSection()">Next</button>
        </div>
    </div>
</div>
<div class="hidden" id="ques">
<?php
echo $this->element('question');
?>
</div>

<script type="text/javascript">
function FuncNextSection(){
	$('div#profit').addClass('hidden');
	$('div#ques').removeClass('hidden');
}
</script>