<div class="container">
    <!--<div id="IndexSuresi" data-timer="180" style="width: 250px; margin-right: auto; margin-left:auto"></div>-->
	<h3 class="text-center">INSTRUCTIONS</h3>
	<div class="divclass firstp">
		<p>You will now play 10 rounds of a “game”. </p>
		<p>At the beginning of each round you will be randomly assigned to a role. There are two possible roles in the game: “First Mover” and “Second Mover”. Each round is the same.</p>
		<p class="text-right"><button type="button" class="btn btn-xs btn-success" onclick="FuncDivAc('secondp')">Next <i class="fa fa-chevron-right"></i></button></p>
	</div>
	<div class="divclass secondp hidden">
		<p>If you are playing as a “First Mover”, in stage 1 you will be given 100 Experimental Points (equivalent to $ 0,20). You can decide to send any amount of Experimental Points to another person we have randomly matched you with. This amount will be multiplied by 3. The other person will then decide whether to return part of the Experimental points or not. Notice that you will play with the same person only once across all rounds.</p>
		<p>In stage 2, you will be able to visualize a short summary of the round.</p>
		<p><div class="row">
			<div class="col-xs-6"><button type="button" class="btn btn-xs btn-danger" onclick="FuncDivAc('firstp')"><i class="fa fa-chevron-left"></i> Back</button></div>
			<div class="col-xs-6 text-right"><button type="button" class="btn btn-xs btn-success" onclick="FuncDivAc('thirdp')">Next <i class="fa fa-chevron-right"></i></button></div>
		</div></p>
	</div>
	<div class="divclass thirdp hidden">
		<p>If you are playing as a “Second Mover”, you will receive a certain amount of Experimental Points from a player you have been randomly matched with. The amount originally sent by the other player is multiplied by 3. You can decide to return or not any amount of the Experimental Points to the other person. Notice that you will play with the same person only once across all rounds.</p>
		<p>In stage 2, you will be able to visualize a short summary of the round.</p>
		<p><div class="row">
			<div class="col-xs-6"><button type="button" class="btn btn-xs btn-danger" onclick="FuncDivAc('secondp')"><i class="fa fa-chevron-left"></i> Back</button></div>
			<div class="col-xs-6 text-right"><button type="button" class="btn btn-xs btn-success" onclick="FuncKontrolEt()">Next <i class="fa fa-chevron-right"></i></button></div>
		</div></p>
	</div>
</div>
<div id="bekle" style="display:none"><h1>The round is starting.<br>Please Wait.</h1></div>
<script type="text/javascript">
    /*$(window).bind('beforeunload',function(){
        $.ajax({
            async:false,
            type:'POST',
            url:'<?php echo $this->Html->url('/');?>surveys/indexsurekaydet',
            data:'time='+parseInt($("#IndexSuresi").TimeCircles().getTime())
        }).done(function(data){
            var dat = $.parseJSON(data);
        }).fail(function(){

        });
        return 'are you sure you want to leave?';
    });*/

$(document).ready(function(){
	<?php if($user['User']['durum'] == 1 && !empty($mIdVarmi)){ ?>
	FuncKontrolEt();
	<?php } ?>
    /*$("#IndexSuresi").data('timer', "<?php echo $user['User']['zaman']; ?>");
    $("#IndexSuresi").TimeCircles({ time: { Days: { show: false }, Hours: { show: false } }})
        .addListener(function(unit, amount, total){
        if(total == 0) {
			$("#IndexSuresi").TimeCircles().stop();
            FuncKontrolEt();
        }
    });*/
});

//setInterval(function(){alert(parseInt($("#IndexSuresi").TimeCircles().getTime()));},5000);
//setTimeout(function(){FuncKontrolEt();},180000);

function FuncKontrolEt(){
    $.blockUI({ message: $('#bekle') });
    setInterval(function(){
        $.ajax({
            async:false,
            type: 'POST',
            url: '<?php echo $this->Html->url('/');?>surveys/OyuncuTamamMi'
        }).done(function(data){
            var dat = $.parseJSON(data);
            if(dat['hata']){
				if(dat['full']){
					alert('This game is full now.');
					window.location.href = '<?php echo $this->Html->url('/');?>pages/home';
				}
            }else{
				window.location.href = '<?php echo $this->Html->url('/');?>surveys/surveyone/m:'+dat['m']+'/i:'+dat['i'];
			}
        }).fail(function(){
            alert('Please try again.');
			return false;
        });
    },1500);
}
function FuncDivAc(div){
	$('.divclass').addClass('hidden');
	$('.'+div).removeClass('hidden');
}
</script>