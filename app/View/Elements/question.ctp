<?php
echo $this->Html->css('datepicker/bootstrap-datepicker3');
echo $this->Html->script('bootstrap-datepicker.min');
?>
<div class="container padRL15">
    <form id="questionform" class="form-horizontal" style="margin-top: 10px;">
        <div id="form-1" class="questions">
            <div class="form-group">
                <label for="gender" class="col-xs-12 col-form-label">Q1. Gender:</label>
            </div>
            <div class="form-group">
                <div class="col-xs-12">
                    <div class="radio">
                        <label>
                            <input type="radio" name="gender" value="male">
                            Male
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="gender" value="female">
                            Female
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-group text-right">
                <button type="button" class="btn btn-primary" onclick="FuncNext('#form-2');">Next</button>
            </div>
        </div>

        <div id="form-2" class="questions hidden">
            <div class="form-group">
                <label for="grade" class="col-xs-12 col-form-label">Q2.a What is the highest grade of school or year of college you have completed?</label>
            </div>
            <div class="form-group">
                <div class="col-xs-12">
                    <div class="radio">
                        <label>
                            <input type="radio" name="grade" value="1" >
                            1 - Less than high school (Grade 11 or less)
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="grade" value="2">
                            2 - High school diploma (including GED)
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="grade" value="3">
                            3 - Some college
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="grade" value="4">
                            4 - Assoc. degree (2 year) or specialized technical training
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="grade" value="5">
                            5 - Bachelor's degree
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="grade" value="6">
                            6 - Some graduate training
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="grade" value="7">
                            7 - Graduate or professional degree
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-6">
                    <button type="button" class="btn btn-danger" onclick="FuncNext('#form-1');">Back</button>
                </div>
                <div class="col-xs-6 text-right">
                    <button type="button" class="btn btn-primary" onclick="FuncNext('#form-3');">Next</button>
                </div>
            </div>
        </div>

        <div id="form-3" class="questions hidden">
            <div class="form-group">
                <label for="principal" class="col-xs-12 col-form-label">Q3. Principal occupation:</label>
            </div>
            <div class="form-group">
                <div class="col-xs-12">
                    <div class="radio">
                        <label>
                            <input type="radio" name="principal" value="1" >
                            1 - Paid employment
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="principal" value="2">
                            2 - Student/attend university
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="principal" value="3">
                            3 - Work or assist in family business
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="principal" value="4">
                            4 - Autonomous professional, freelancer, or self-employed
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="principal" value="5">
                            5 - Job seeker following job loss
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="principal" value="6">
                            6 - First-time job seeker
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="principal" value="7">
                            7 - Exempted from job seeking following job loss
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="principal" value="8">
                            8 - Take care of the housekeeping
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="principal" value="9">
                            9 - Retired/pensioner
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="principal" value="10">
                            10 - Perform unpaid work while retaining unemployment benefit
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="principal" value="11">
                            11 - Perform voluntary work
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="principal" value="12">
                            12 - Does something else
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-6">
                    <button type="button" class="btn btn-danger" onclick="FuncNext('#form-2');">Back</button>
                </div>
                <div class="col-xs-6 text-right">
                    <button type="button" class="btn btn-primary" onclick="FuncNext('#form-4');">Next</button>
                </div>
            </div>
        </div>

        <div id="form-4" class="questions hidden">
            <div class="form-group">
                <label for="race" class="col-xs-12 col-form-label">Q4. Do you consider yourself to be White, Black or African American, Asian or Pacific Islander, Native American, or some other race?</label>
            </div>
            <div class="form-group">
                <div class="col-xs-12">
                    <div class="radio">
                        <label>
                            <input type="radio" name="race" value="1" >
                            1 - White
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="race" value="2">
                            2 - African American or Black
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="race" value="3">
                            3 - Asian or Pacific Islander
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="race" value="4">
                            4 - Alaskan Native/Native American
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="race" value="5">
                            5 - Other
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-6">
                    <button type="button" class="btn btn-danger" onclick="FuncNext('#form-3');">Back</button>
                </div>
                <div class="col-xs-6 text-right">
                    <button type="button" class="btn btn-primary" onclick="FuncNext('#form-5');">Next</button>
                </div>
            </div>
        </div>

        <div id="form-5" class="questions hidden">
            <div class="form-group">
                <label for="dbirth" class="col-xs-12 col-form-label">Q5. Date of birth: </label>
            </div>
            <div class="form-group">
                <div class="col-xs-12">
                    <input type="text" class="form-control datepicker" name="dbirth" readonly>
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-6">
                    <button type="button" class="btn btn-danger" onclick="FuncNext('#form-4');">Back</button>
                </div>
                <div class="col-xs-6 text-right">
                    <button type="button" class="btn btn-primary" onclick="FuncNext('#form-6');">Next</button>
                </div>
            </div>
        </div>

        <div id="form-6" class="questions hidden">
            <div class="form-group">
                <label for="risk" class="col-xs-12 col-form-label">Q6. Are you generally a person who is fully prepared to take risks or do you try to avoid taking risks? Please tick a box on the scale,where 0 means 'unwilling to take risks' and the value 10 means 'fully prepared to take risks'.</label>
            </div>
            <div class="form-group">
                <div class="col-xs-12">
                    <div class="radio">
                        <label>
                            <input type="radio" name="risk" value="0" >
                            0 - Unwilling to take risks
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="risk" value="1">
                            1
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="risk" value="2">
                            2
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="risk" value="3">
                            3
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="risk" value="4">
                            4
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="risk" value="5">
                            5
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="risk" value="6">
                            6
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="risk" value="7">
                            7
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="risk" value="8">
                            8
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="risk" value="9">
                            9
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="risk" value="10">
                            10 - Fully prepared to take risks
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-6">
                    <button type="button" class="btn btn-danger" onclick="FuncNext('#form-5');">Back</button>
                </div>
                <div class="col-xs-6 text-right">
                    <button type="button" class="btn btn-primary" onclick="FuncNext('#form-7');">Next</button>
                </div>
            </div>
        </div>

        <div id="form-7" class="questions hidden">
            <div class="form-group">
                <label for="careful" class="col-xs-12 col-form-label">Q7. Generally speaking, would you say that most people can be trusted, or that you can't be too careful in dealing with people?</label>
            </div>
            <div class="form-group">
                <div class="col-xs-12">
                    <div class="radio">
                        <label>
                            <input type="radio" name="careful" value="0" >
                            0 - You can't be too careful
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="careful" value="1">
                            1
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="careful" value="2">
                            2
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="careful" value="3">
                            3
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="careful" value="4">
                            4
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="careful" value="5">
                            5
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="careful" value="6">
                            6
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="careful" value="7">
                            7
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="careful" value="8">
                            8
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="careful" value="9">
                            9
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="careful" value="10">
                            10 - Most people can be trusted
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-6">
                    <button type="button" class="btn btn-danger" onclick="FuncNext('#form-6');">Back</button>
                </div>
                <div class="col-xs-6 text-right">
                    <button type="button" class="btn btn-primary" onclick="FuncNext('#form-8');">Next</button>
                </div>
            </div>
        </div>
        <div id="form-8" class="questions hidden">
            <div class="form-group">
                <label for="trustpeople" class="col-xs-12 col-form-label">Q8. How much do you trust each of the following groups of people</label>
            </div>
            <div class="form-group">
                <label for="neighbourhood" class="col-xs-12 col-form-label">Q8.a People in your neighbourhood?</label>
            </div>
            <div class="form-group">
                <div class="col-xs-12">
                    <div class="radio">
                        <label>
                            <input type="radio" name="neighbourhood" value="1" >
                            1 - Cannot be trusted at all
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="neighbourhood" value="2">
                            2
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="neighbourhood" value="3">
                            3
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="neighbourhood" value="4">
                            4
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="neighbourhood" value="5">
                            5 - Can be trusted a lot
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="strangers" class="col-xs-12 col-form-label">Q8.b Strangers?</label>
            </div>
            <div class="form-group">
                <div class="col-xs-12">
                    <div class="radio">
                        <label>
                            <input type="radio" name="strangers" value="1" >
                            1 - Cannot be trusted at all
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="strangers" value="2">
                            2
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="strangers" value="3">
                            3
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="strangers" value="4">
                            4
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="strangers" value="5">
                            5 - Can be trusted a lot
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-6">
                    <button type="button" class="btn btn-danger" onclick="FuncNext('#form-7');">Back</button>
                </div>
                <div class="col-xs-6 text-right">
                    <button type="button" class="btn btn-primary" onclick="FuncNext('#form-9');">Next</button>
                </div>
            </div>
        </div>
        <div id="form-9" class="questions hidden">
            <div class="form-group">
                <label class="col-xs-12 col-form-label">Q9. Now a brief description will be provided. Please read the following description and tick a box to indicate how much each person is or is not like you.</label>
            </div>
            <div class="form-group">
                <label for="care" class="col-xs-12 col-form-label">Q9.a “It is important to her/him to help the people around her/him. She/he wants to care for their well-being.</label>
            </div>
            <div class="form-group">
                <div class="col-xs-12">
                    <div class="radio">
                        <label>
                            <input type="radio" name="care" value="1" >
                            Very much like me
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="care" value="2">
                            Like me
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="care" value="3">
                            Somewhat like me
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="care" value="4">
                            A little like me
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="care" value="5">
                            Not like me
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="care" value="6">
                            Not like me at all
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-6">
                    <button type="button" class="btn btn-danger" onclick="FuncNext('#form-8');">Back</button>
                </div>
                <div class="col-xs-6 text-right">
                    <button id="gonder" type="button" class="btn btn-success">Submit</button>
                </div>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('.datepicker').datepicker({
            format: 'dd.mm.yyyy',
            autoclose: true,
            endDate: '31.12.1998',
			startDate:'01.01.1916'
        });

        $('.number').mask("00000");

        $('#questionform #gonder').on('click',function(){
			if($('input[name="care"]:checked').length == 0){
				alert('Please check it.');	
			}else{
				$.blockUI();
				setTimeout(function(){FuncQuestionFormSend();},500);
			}            
        });
    });

    function FuncQuestionFormSend(){
        $.ajax({
            async: false,
            type: "POST",
            url: "<?php echo $this->Html->url('/').$this->params['controller']; ?>/questioncevap",
            data: $('form#questionform').serialize()+'&m=<?php echo $UserM; ?>&i=<?php echo $UserI; ?>'
        }).done(function(data){
            var dat = $.parseJSON(data);
            if(dat['hata']){
                if(dat['sonuc'] == 3){
                    window.location.href = "<?php echo $this->Html->url('/');?>pages/home";
                }else{
                    window.location.reload();
                }
            }else{
                window.location.href = "<?php echo $this->Html->url('/').$this->params['controller'];?>/oyunbitti/m:"+dat['m']+'/i:'+dat['i'];
            }
        }).fail(function(){
            window.location.reload();
        });
    }

    function FuncNext(formId){
		if(formId == '#form-2' && $('input[name="gender"]:checked').length == 0){
			$.blockUI({message:'<h4>Please choose an option to continue.<h4><i class="text-danger fa fa-times fa-2x" onclick="$.unblockUI();"></i> <h5>Click Here.<h5>',onOverlayClick: $.unblockUI});
//			alert('Please choose an option to continue.');
		}else if(formId == '#form-3' && $('input[name="grade"]:checked').length == 0){
			$.blockUI({message:'<h4>Please choose an option to continue.<h4><i class="text-danger fa fa-times fa-2x" onclick="$.unblockUI();"></i> <h5>Click Here.<h5>',onOverlayClick: $.unblockUI});
		}else if(formId == '#form-4' && $('input[name="principal"]:checked').length == 0){
			$.blockUI({message:'<h4>Please choose an option to continue.<h4><i class="text-danger fa fa-times fa-2x" onclick="$.unblockUI();"></i> <h5>Click Here.<h5>',onOverlayClick: $.unblockUI});
		}else if(formId == '#form-5' && $('input[name="race"]:checked').length == 0){
			$.blockUI({message:'<h4>Please choose an option to continue.<h4><i class="text-danger fa fa-times fa-2x" onclick="$.unblockUI();"></i> <h5>Click Here.<h5>',onOverlayClick: $.unblockUI});
		}else if(formId == '#form-6' && $('input[name="dbirth"]').val() == ''){
			$.blockUI({message:'<h4>Please choose an option to continue.<h4><i class="text-danger fa fa-times fa-2x" onclick="$.unblockUI();"></i> <h5>Click Here.<h5>',onOverlayClick: $.unblockUI});
		}else if(formId == '#form-7' && $('input[name="risk"]:checked').length == 0){
			$.blockUI({message:'<h4>Please choose an option to continue.<h4><i class="text-danger fa fa-times fa-2x" onclick="$.unblockUI();"></i> <h5>Click Here.<h5>',onOverlayClick: $.unblockUI});
		}else if(formId == '#form-8' && $('input[name="careful"]:checked').length == 0){
			$.blockUI({message:'<h4>Please choose an option to continue.<h4><i class="text-danger fa fa-times fa-2x" onclick="$.unblockUI();"></i> <h5>Click Here.<h5>',onOverlayClick: $.unblockUI});
		}else if(formId == '#form-9' && ($('input[name="neighbourhood"]:checked').length == 0 || $('input[name="strangers"]:checked').length == 0)){
			$.blockUI({message:'<h4>Please choose an option to continue.<h4><i class="text-danger fa fa-times fa-2x" onclick="$.unblockUI();"></i> <h5>Click Here.<h5>',onOverlayClick: $.unblockUI});
		}else{
			$('.questions').addClass('hidden');
			$(formId).removeClass('hidden');
		}        
    }
    function FuncBack(formId){
        $('.questions').addClass('hidden');
        $(formId).removeClass('hidden');
    }
</script>
